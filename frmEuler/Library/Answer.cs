﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion references

namespace Euler.Library
{
	class Answer
	{
		#region variables
		private int no;
		private int time;
		private string answer;
		#endregion variables

		#region initialization
		/// <summary>Initializes an answer.</summary>
		public Answer(int inNo, string inAns, int inTime)
		{
			no = inNo;
			answer = inAns;
			time = inTime;
		}
		#endregion initialization

		#region accessors and mutators
		public int getNo() { return no; }
		public int getTime() { return time; }
		#endregion accessors and mutators

		#region I/O functions
		/// <summary>Returns an output-friendly string.</summary>
		public string toString()
		{
			return no.ToString() + Problem.separator1 + answer +
				Problem.separator2 + time.ToString();
		}
		/// <summary>Converts an array of Answers to a List, sorts them, and converts it back.</summary>
		public static void sort(ref Answer[] array)
		{
			for (int outdex = 0; outdex < array.Length; outdex++)
				for (int index = 0; index < array.Length - 1; index++)
					if (array[index].getNo() > array[index + 1].getNo())
						swap(ref array[index], ref array[index + 1]);
		}
		/// <summary>Swaps two Answer objects.</summary>
		private static void swap(ref Answer x, ref Answer y)
		{
			Answer temp = x;
			x = y;
			y = temp;
		}
		#endregion I/O functions
	}
}