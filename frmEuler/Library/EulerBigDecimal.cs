﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion references

namespace Euler.Library
{
	class EulerBigDecimal
	{
		#region variables
		private string value;
		#endregion variables

		#region initialization
		/// <summary>Approximates a fraction with extremely long decimal numbers.</summary>
		/// <param name="num">Numerator.</param>
		/// <param name="denom">Denominator.</param>
		/// <param name="accuracy">The number of digits, including coefficient, desired for accuracy.  Defaults to the denominator.</param>
		public EulerBigDecimal(int num, int denom, int accuracy = 0)
		{
			if (accuracy == 0)
				accuracy = denom;
			bool repeating = false;
			bool flagged = false;
			string r = "";	// Remainder
			string f = "";	// Float
			string coefficient = "";

			// Find the coefficient and filter it out.
			if (num > denom)
				longDivide(ref num, denom, ref coefficient, ref repeating);
			else
				coefficient += '0';
			coefficient += '.';

			num *= 10;
			// Calculate the full remainder.
			while (!repeating && r.Length + f.Length < accuracy)
			{
				char c = longDivide(ref num, denom, ref r, ref repeating);
				if (c == r[0] && r.Length > 1)
				{
					Swapper.swap(ref r, ref f);
					r += c;
					flagged = true;
				}
				if (flagged)
				{
					while (r.Length < f.Length)
					{
						longDivide(ref num, denom, ref r, ref repeating);
						if (r[r.Length - 1] != f[r.Length - 1])
							break;
					}
					if (r.Equals(f))
						repeating = true;
					else
					{
						f += r.Substring(1);
						Swapper.swap(ref r, ref f);
					}
					flagged = false;
				}
				f = "";
			}
			value = coefficient + r.Remove(r.Length - 1);
		}
		#endregion initialization

		#region I/O functions
		/// <summary>Returns a string representation of the number.</summary>
		public override string ToString()	{	return value.ToString();	}
		#endregion I/O functions

		#region private functions
		/// <summary>Performs long division and sets done to true if there is no remainder.</summary>
		private char longDivide(ref int dividend, int divisor, ref string remainder, ref bool done)
		{
			string coefficient = (dividend / divisor).ToString();
			remainder += coefficient;
			dividend %= divisor;
			if (dividend == 0)
				done = true;
			else
				dividend *= 10;
			return coefficient[0];
		}
		#endregion private functions
	}
}