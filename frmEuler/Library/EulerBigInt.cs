﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
#endregion references

namespace Euler.Library
{
	class EulerBigInt
	{
		#region conversion functions
		/// <summary>Converts an array of strings to an array of BigIntegers.</summary>
		private static BigInteger[] parse(string[] terms)
		{
			BigInteger[] result = new BigInteger[terms.Length];
			for (int index = 0; index < terms.Length; index++)
				result[index] = BigInteger.Parse(terms[index]);
			return result;
		}
		/// <summary>Converts a string to a BigInteger.</summary>
		public static BigInteger toBigInt(string input)
		{
			return BigInteger.Parse(input);
		}
		#endregion conversion functions

		#region special math functions
		/// <summary>Finds a term of the Fibonnacci sequence with a specified restriction.  Note that if termCount and length are both sent as parameters, termCount will not be used.  Returns the next Fibonnaci number if called with only counter set as a parameter.</summary>
		/// <param name="counter">Keeps track of how many times F(n) has been called.</param>
		/// <param name="t1">First term of the sequence.</param>
		/// <param name="t2">Second term of the sequence.</param>
		/// <param name="termCount">How many times to run F(n).</param>
		/// <param name="length">How many digits the term should have.</param>
		/// <returns>The term the function ended on.</returns>
		public static string getFibonnacci(ref int counter, string t1 = "1", string t2 = "1", int length = 0, int termCount = 1)
		{
			BigInteger[] result = parse(new string[] { t1, t2 });
			counter += 2;	// Offset since the first two terms are given.

			// Length has been specified.  Find all the first term with (length) digits.
			if (length > 0)
				while (result[1].ToString().Length < length)
				{
					result[0] += result[1];
					if (result[0].ToString().Length < length)
					{
						result[1] += result[0];
						counter += 2;
					}
					else
					{
						Swapper.swap(ref result[0], ref result[1]);
						counter++;
					}
				}
			// Length has not been specified.  Find (termCount) terms, or the next term if not specified.
			else
				for (int index = 0; index < termCount; index++)
				{
					result[0] += result[1];
					if (++index < termCount)
					{
						result[1] += result[0];
						counter += 2;
					}
					else
					{
						Swapper.swap(ref result[0], ref result[1]);
						counter++;
					}
				}
			return result[1].ToString();
		}
		/// <summary>Combinatorics function to find how many ways to select r objects from a collection of n objects.</summary>
		/// <param name="n">Must be greater than 0.</param>
		/// <param name="r">Must be less than or equal to n.</param>
		/// <returns>The number of unique object combinations where order does not matter.</returns>
		public static string ncr(int n, int r)
		{
			return 
				EulerBigInt.divide(
					EulerBigInt.factorial(n.ToString()),
					EulerBigInt.multiply(
						EulerBigInt.factorial(r.ToString()),
						EulerBigInt.factorial((n - r).ToString())));
		}
		#endregion special math functions

		#region standard math functions
		/// <summary>Returns the factorial of a number.</summary>
		public static string factorial(string input)
		{
			BigInteger result = 1;
			BigInteger multiplier = BigInteger.Parse(input);

			while (multiplier > 1)
				result = BigInteger.Multiply(result, multiplier--);
			return result.ToString();
		}
		// Need to find a way to make this more usable in the future.
		public static string divide(string dividend, string divisor)
		{
			return BigInteger.Divide(
				BigInteger.Parse(dividend),
				BigInteger.Parse(divisor)).ToString();
		}
		// Need to find a way to make this more usable in the future.
		public static string multiply(string multiplicand, string multiplier)
		{
			return BigInteger.Multiply(
					BigInteger.Parse(multiplicand),
					BigInteger.Parse(multiplier)).ToString();
		}
		/// <summary>Returns a number raised to a power.  Squares a number if no exponent is specified.</summary>
		public static string pow(string input, int exponent = 2)
		{
			return BigInteger.Pow(BigInteger.Parse(input), exponent).ToString();
		}
		/// <summary>Subtracts any number of integers from another number.</summary>
		public static string subtract(string input, params string[] subtractors)
		{
			BigInteger result = BigInteger.Parse(input);
			foreach (string s in subtractors)
				result -= BigInteger.Parse(s);
			return result.ToString();
		}
		/// <summary>Sums up a list or an array of strings.</summary>
		public static string sum(List<string> input = null, params string[] add)
		{
			BigInteger sum = 0;
			if (input != null)
				foreach (string s in input)
					sum += BigInteger.Parse(s);
			else
				foreach (string s in add)
					sum += BigInteger.Parse(s);
			return sum.ToString();
		}
		#endregion standard math functions
	}
}