﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion references

namespace Euler.Library
{
	static class EulerMath // Fib improvements
	{
		#region boolean tests
		/// <summary>Tests whether or not a number is an integer (math definition).</summary>
		public static bool isInteger(float num) { return num == (int)num; }
		/// <summary>Tests whether or not a number is an integer (math definition).</summary>
		public static bool isInteger(double num) { return num == (long)num; }
		/// <summary>Tests whether or not a number is pandigital.</summary>
		/// <param name="input">String representation of a number.</param>
		/// <param name="excl">Numbers to exclude, defaults to "0".</param>
		/// <param name="numDigits">The base of the number.  For hex, make this 16.</param>
		public static bool isPandigital(string input, string excl = "0", int numDigits = 10)
		{
			if (input.Length == numDigits - excl.Length)
			{
				foreach (char c in excl)
					if (input.Contains(c))				// Contains an invalid number.
						return false;
				if (!EulerString.repeatsChars(input))	// No repeating digits.
					return true;
			}
			return false;
		}
		/// <summary>Finds all factors of a number to determine whether or not a number is prime.  If the factors are only 1 and the number, the number is prime.</summary>
		public static bool isPrime(int num)
		{
			if (factor(num).Count == 2)
				return true;
			return false;
		}
		/// <summary>Tests if a number is truncatable.  If after cutting a number off, the resulting number is contained in "pool", then the number is truncatable in that direction.</summary>
		/// <param name="input">Number to be truncated.</param>
		/// <param name="pool">Pool of valid numbers.</param>
		/// <param name="direction">Negative truncates left side.  Positive truncates right.  Defaults to zero, which performs both truncations.</param>
		public static bool isTruncatable(int input, List<int> pool, int direction = 0)
		{
			string temp = input.ToString();
			if (temp.Length < 2)
				return false;
			// Truncate the left side until empty.
			if (direction <= 0)
				while (temp.Length > 1)
				{
					temp = temp.Substring(1);
					if (!pool.Contains(int.Parse(temp)))
						return false;
				}
			temp = input.ToString();
			// Truncate the right side until empty.
			if (direction >= 0)
				while (temp.Length > 1)
				{
					temp = temp.Remove(temp.Length - 1);
					if (!pool.Contains(int.Parse(temp)))
						return false;
				}
			return true;
		}
		/// <summary>Tests two integers and finds out if they have the share the same digits.  Order does not matter.</summary>
		public static bool hasSameDigits(int x, int y)
		{
			int[] nx = new int[10];				// Counts of each digit in x.
			int[] ny = new int[10];				// Counts of each digit in y.

			string sx = x.ToString();			// String representation of x.
			string sy = y.ToString();			// String representation of y.

			foreach (char c in sx)				// Tally the digits of x.
				nx[int.Parse(c.ToString())]++;
			foreach (char c in sy)				// Tally the digits of y.
				ny[int.Parse(c.ToString())]++;

			for (int index = 0; index < 10; index++)
				if (nx[index] - ny[index] != 0)
					return false;				// Return false if there is a difference between nx and ny.
			return true;
		}
		#endregion boolean tests

		#region conversion functions
		/// <summary>Turns a char into an integer.</summary>
		public static int charToInt(char input) { return input - 48; }
		/// <summary>Converts a base 10 integer to a binary string.</summary>
		public static string decToBinary(int input)	{	return Convert.ToString(input, 2);	}
		#endregion conversion functions

		#region n-tagonal numbers
		/// <summary>Tests whether or not a number is triangular.</summary>
		public static bool isTri(long num) { return EulerMath.isInteger(itri(num)); }
		/// <summary>Tests whether or not a number is pentagonal.</summary>
		public static bool isPent(long num) { return EulerMath.isInteger(ipent(num)); }
		/// <summary>Tests whether or not a number is hexagonal.</summary>
		public static bool isHex(long num) { return EulerMath.isInteger(ihex(num)); }
		/// <summary>Inverse triangular number function.</summary>
		private static double itri(long num) { return (Math.Sqrt(8 * num + 1) + 1) / 2; }
		/// <summary>Inverse pentagonal number function.</summary>
		private static double ipent(long num) { return (Math.Sqrt(24 * num + 1) + 1) / 6; }
		/// <summary>Inverse hexagonal number function.</summary>
		private static double ihex(long num) { return (Math.Sqrt(8 * num + 1) + 1) / 4; }
		/// <summary>Finds the nth triangular number.</summary>
		public static long tri(long n) { return n * (n + 1) / 2; }
		/// <summary>Finds the nth pentagonal number.</summary>
		public static long pent(long n) { return n * (3 * n - 1) / 2; }
		/// <summary>Finds the nth hexagonal number.</summary>
		public static long hex(long n) { return n * (2 * n - 1); }
		#endregion n-tagonal numbers

		#region prime calculations
		/// <summary>Returns an array of Boolean values.  True if prime, false if not.  Faster than getPrimes for large numbers.</summary>
		public static bool[] getPrimeBooleans(int max)
		{
			bool[] prime = Enumerable.Repeat<bool>(true, max + 1).ToArray();

			// 0 and 1 are not prime.
			prime[0] = prime[1] = false;

			// Eliminate all multiples of 2.
			for (int i = 4; i < max; i += 2)
				prime[i] = false;

			// Move up to the max, finding primes.
			for (int outdex = 3; outdex <= max; outdex += 2)
				// Outdex is prime.
				if (prime[outdex] && outdex < Math.Sqrt(max))
					// Eliminate its multiples.
					for (int index = outdex * outdex; index <= max; index += outdex * 2)
						prime[index] = false;
			return prime;
		}
		/// <summary>Uses Sieve of Eratosthenes to acquire prime numbers up to a max or a certain number of primes.  Specify either a max or a count desired..</summary>
		public static List<int> getPrimes(int max = 0, int count = 0)
		{
			List<int> primes = new List<int>() { 2 };
			if (max != 0)
			{

				double sqrt = Math.Sqrt(max);
				bool[] eliminated = new bool[max + 1];

				for (int outdex = 3; outdex <= max; outdex += 2)
					if (!eliminated[outdex])
					{
						if (outdex < sqrt)
							for (int index = outdex * outdex; index <= max; index += outdex * 2)
								eliminated[index] = true;
						primes.Add(outdex);
					}
			}
			else if (count != 0)
			{
				for (int outdex = 3; primes.Count < count; outdex += 2)
				{
					bool isPrime = true;
					for (int index = 0; index < primes.Count && isPrime; index++)
						if (outdex % primes[index] == 0)
							isPrime = false;
					if (isPrime)
						primes.Add(outdex);
				}
			}
			return primes;
		}
		/// <summary>Returns a prime factorization of a number.</summary>
		public static List<int> primeFactor(int input)
		{
			List<int> factors = new List<int>();
			List<int> result = new List<int>();

			if (input > 2)
			{
				if (input % 2 == 0)
				{
					factors.Add(2);
					input /= 2;
				}
				for (int index = 3; index <= input; index += 2)
					// Remove all factors which are powers of index.
					while (input % index == 0)
					{
						if (!factors.Contains(index))
							factors.Add(index);
						input /= index;
					}
				return factors;
			}
			else if (input == 2)
				return new List<int>() { input };
			return null;
		}
		/// <summary>Returns a prime factorization of a number.</summary>
		public static List<long> primeFactor(long input)
		{
			List<long> factors = new List<long>();

			if (input > 2)
			{
				if (input % 2 == 0)
				{
					factors.Add(2);
					input /= 2;
				}
				for (long index = 3; index <= input; index += 2)
					// Remove all factors which are powers of index.
					while (input % index == 0)
					{
						if (!factors.Contains(index))
							factors.Add(index);
						input /= index;
					}
				return factors;
			}
			else if (input == 2)
				return new List<long>() { input };
			return null;
		}
		/// <summary>Adds the next prime given a list of primes.</summary>
		public static void addNextPrime(ref List<int> primes)
		{
			bool added = false;
			if (primes.Count == 0)
				primes = new List<int>() { 2, 3 };
			int current = primes[primes.Count - 1];

			while (!added)
			{
				current += 2;
				// Test if the current number is divisible by a previous prime.
				for (int index = 0; index < primes.Count; index++)
					// Current number is not prime.
					if (current % primes[index] == 0)
						break;
					// If index hit the last prime, then the current number is prime.
					else if (index == primes.Count - 1)
						added = true;
			}
			primes.Add(current);
		}
		#endregion prime calculations

		#region special math functions
		/// <summary>Returns a list of palindromic numbers up to a maximum.</summary>
		public static List<int> getPalindromes(int max)
		{
			List<int> palindromes = new List<int>();
			for (int index = 0; index < max; index++)
				if (EulerString.isPalindrome(index.ToString()))
					palindromes.Add(index);
			return palindromes;
		}
		/// <summary>Creates a Fibonacci sequence given some restrictions.  Should be called using named parameters to avoid confusion.  Multiple restrictions are allowed simultaneously.</summary>
		/// <param name="maxValue">Create a sequence up to a certain number.</param>
		/// <param name="numElements">Create a sequence of x elements.</param>
		public static List<int> getFibonacci(int maxValue = 0, short numElements = 0)
		{
			List<int> fib = new List<int>() { 1, 2 };

			// Both restrictions are in place.
			if (maxValue != 0 && numElements != 0)
				while (fib[fib.Count - 1] < maxValue && fib.Count < numElements - 2)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Add terms until maxValue is reached.
			else if (maxValue != 0)
				while (fib[fib.Count - 1] < maxValue)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Add terms until the number of elements is reached.
			else if (numElements != 0)
				while (fib.Count < numElements)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Compensate for the extra calculation on maxValue if there was one.
			if (maxValue != 0 && fib[fib.Count - 1] > maxValue)
				fib.RemoveAt(fib.Count - 1);
			return fib;
		}		
		/// <summary>Creates a Fibonacci sequence given some restrictions.  Should be called using named parameters to avoid confusion.  Multiple restrictions are allowed simultaneously.</summary>
		/// <param name="maxValue">Create a sequence up to a certain number.</param>
		/// <param name="numElements">Create a sequence of x elements.</param>
		public static List<long> getFibonacci(long maxValue = 0, int numElements = 0)
		{
			List<long> fib = new List<long>() { 1, 2 };

			// Both restrictions are in place.
			if (maxValue != 0 && numElements != 0)
				while (fib[fib.Count - 1] < maxValue && fib.Count < numElements - 2)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Add terms until maxValue is reached.
			else if (maxValue != 0)
				while (fib[fib.Count - 1] < maxValue)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Add terms until the number of elements is reached.
			else if (numElements != 0)
				while (fib.Count < numElements)
					fib.Add(fib[fib.Count - 1] + fib[fib.Count - 2]);
			// Compensate for the extra calculation on maxValue if there was one.
			if (maxValue != 0 && fib[fib.Count - 1] > maxValue)
				fib.RemoveAt(fib.Count - 1);
			return fib;
		}
		#endregion special math functions

		#region standard math functions
		/// <summary>Finds all factors of a number and returns them in a List.</summary>
		public static List<int> factor(int input)
		{
			List<int> factors = new List<int>();

			for (int index = 1; index <= Math.Sqrt(input); index++)
				if (input % index == 0)
				{
					factors.Add(index);
					if (!factors.Contains(input / index))
						factors.Add(input / index);
				}
			factors.Sort();
			return factors;
		}
		/// <summary>Finds all factors of a number and returns them in a List.</summary>
		public static List<long> factor(long input)
		{
			List<long> factors = new List<long>();

			for (long index = 1; index <= Math.Sqrt(input); index++)
				if (input % index == 0)
				{
					factors.Add(index);
					if (!factors.Contains(input / index))
						factors.Add(input / index);
				}
			factors.Sort();
			return factors;
		}
		/// <summary>Returns the factorial of a number.</summary>
		public static long factorial(int input)
		{
			if (input == 0)
				return 1;
			long product = input;
			while (--input > 0)
				product *= input;
			return product;
		}
		/// <summary>Sums all elements of a list.</summary>
		public static int sum(List<int> list)
		{
			int sum = 0;
			foreach (int i in list)
				sum += i;
			return sum;
		}
		/// <summary>Sums all elements of a queue.</summary>
		public static int sum(Queue<int> q)
		{
			int sum = 0;
			foreach (int i in q)
				sum += i;
			return sum;
		}
		/// <summary>Sums all elements of a list.</summary>
		public static long sum(List<long> list)
		{
			long sum = 0;
			foreach (long i in list)
				sum += i;
			return sum;
		}
		#endregion standard math functions

		
	}
}