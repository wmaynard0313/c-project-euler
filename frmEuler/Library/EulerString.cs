﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
#endregion references

namespace Euler.Library
{
	static class EulerString
	{
		#region boolean tests
		/// <summary>Tests whether or not input string is palindromic.</summary>
		public static bool isPalindrome(string input)
		{
			for (int index = 0; index <= input.Length / 2; index++)
				if (input[index] != input[(input.Length - 1) - index])
					return false;
			return true;
		}
		/// <summary>Returns true if a string repeats any characters.</summary>
		public static bool repeatsChars(string input)
		{
			while (input.Length > 0)
			{
				char c = input[input.Length - 1];
				input = input.Remove(input.Length - 1);
				if (input.Contains(c))
					return true;
			}
			return false;
		}
		#endregion boolean tests

		#region char functions
		/// <summary>Sums character values of a string.  'A' is 1.</summary>
		public static int sumCharValues(string word)
		{
			int sum = 0;
			foreach (char c in word)
				sum += (byte)c - 64;
			return sum;
		}
		#endregion char functions

		#region I/O functions
		// Separates a text file of format "Text1","Text2","Text3"...
		public static string[] separate(string filename)
		{
			string[] list = File.ReadAllText(
				Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
				@"\Visual Studio 2012\Projects\ProjectEuler\frmEuler\Other\Project Resources\" +
				filename).Split(',');
			for (int index = 0; index < list.Length; index++)
				list[index] = list[index].Remove(list[index].Length - 1).Remove(0, 1);
			sort(ref list);
			return list;
		}
		#endregion I/O functions

		#region miscellaneous functions
		/// <summary>Creates a list of every possible combination of characters.
		/// <param name="results">Should start as a new list.  Contains all the combinations after completing the run.</param>
		/// <param name="input">The string to create combinations from.</param>
		/// <param name="result">Used for recursion.  Do not initialize.</param>	
		public static void getCombos(ref List<string> results, string input, string result = "")
		{
			for (int index = 0; index < input.Length; index++)
			{
				result += input[index];
				getCombos(ref results, input.Remove(index, 1), result);
				result = result.Remove(result.Length - 1);
			}
			if (!(input.Length > 0))
				results.Add(result);
		}
		/// <summary>Converts the array to a list, sorts it, and edits the original array accordingly.</summary>
		public static void sort(ref string[] input)
		{
			List<string> list = new List<string>();
			foreach (string s in input)
				list.Add(s);
			list.Sort();
			for (int index = 0; index < input.Length; index++)
				input[index] = list[index];
		}
		#endregion miscellaneous functions
	}
}