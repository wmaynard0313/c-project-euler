﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;
#endregion references

namespace Euler.Library
{
	class Problem
	{
		#region variables
		public static char separator1 = '|';
		public static char separator2 = ':';
		protected static bool allAnswers = false;
		protected int no;
		protected string name;
		protected string text;
		protected Stopwatch timer;		
		protected Random random;
		protected static string outputFileName = 
			Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
				"Euler Solutions.txt");
		#endregion variables

		#region initialization
		/// <summary>Initializes the problem.</summary>
		protected void init()
		{
			try
			{
				this.no = int.Parse(Regex.Match(this.GetType().Name, @"\d+").Value);
				this.name = "Problem " + this.no;
				this.text = Other.ProblemText.text[this.no - 1];
			}
			catch (Exception) { }

			this.random = new Random();
			this.timer = new Stopwatch();
			this.timer.Start();
		}
		#endregion initialization

		#region I/O functions
		/// <summary>Delete the output File so it can be recreated.</summary>
		public static void deleteOutputFile()
		{
			if (File.Exists(outputFileName))
				File.Delete(outputFileName);
		}
		/// <summary>Creates a Windows Form to display the answer and comments.  If the "allAnswers" boolean is set, instead this appends a text file with the answer.</summary>
		protected void output(string result, string comments)
		{
			this.timer.Stop();
			string time = "Time : " + timer.ElapsedMilliseconds + " ms (" + timer.ElapsedMilliseconds / 1000 + " s) ";

			// Display a titled Windows Form object with the answer.
			if (!allAnswers)
			{
				Other.frmResults form = new Other.frmResults();
				form.setName(this.name);
				form.setAnswer(result, time, comments);
				try
				{
					Clipboard.SetText(result);
				}
				catch (Exception) { }
				Application.Run(form);
			}
			// Append the answer and data to specified output file.
			else
			{
				if (!File.Exists(outputFileName))
					File.Create(outputFileName).Close();
				StreamWriter writer = File.AppendText(outputFileName);
				writer.WriteLine(this.no.ToString() + separator1 + result + separator2 + timer.ElapsedMilliseconds);
				writer.Close();
			}
		}
		/// <summary>Converts any parameter to a string for output, and allows any number of parameters for comments.</summary>
		protected void output(object result, params object[] comments) 
		{
			string comment = "";
			foreach (object o in comments)
				comment += o.ToString() + "\n\n";
			output(result.ToString(), comment); 
		}
		/// <summary>Outputs a problem to a titled text file instead of using the Windows form.</summary>
		protected void outputToFile(string result)
		{
			string filename =
				Path.Combine
				(
					Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
					this.name + " Output.txt"
				);
			if (File.Exists(filename))
				File.Delete(filename);
			System.IO.FileStream fs = File.Create(filename);
			fs.Close();
			File.WriteAllText(filename, result);
			Process.Start(filename);
		}
		/// <summary>Sort the output file with all answers via ascending problem number.</summary>
		public static void sortOutFile()
		{
			string[] lines = File.ReadAllLines(getOutputFileName());
			Answer[] ans = new Answer[lines.Length];
			int totalTime = 0;
			float avgTime;

			// Convert each line to an Answer object and place it into the ans array.
			for (int index = 0; index < lines.Length; index++)
			{
				string s = lines[index];
				int noSeparator = s.IndexOf(separator1);
				int ansSeparator = s.IndexOf(separator2);

				ans[index] = new Answer(
					int.Parse(s.Substring(0, noSeparator++)),
					s.Substring(noSeparator, ansSeparator++ - noSeparator),
					int.Parse(s.Substring(ansSeparator)));
			}
			Answer.sort(ref ans);
			deleteOutputFile();

			// Recreate the output file.
			if (!File.Exists(outputFileName))
				File.Create(outputFileName).Close();
			StreamWriter writer = File.AppendText(outputFileName);
			writer.WriteLine("KEY : (Problem #)" + separator1 +
				"(Answer)" + separator2 + "(Time, in ms)\n");

			// Write out each line to the file, keeping a running total of the time taken.
			foreach (Answer a in ans)
			{
				writer.WriteLine(a.toString());
				totalTime += a.getTime();
			}
			avgTime = (float)totalTime / (float)ans.Length;

			writer.WriteLine('\n' +
				ans.Length.ToString() + " problems took " +
				totalTime.ToString() + "ms (" +
				((float)totalTime / (float)1000).ToString() + "s)");
			writer.WriteLine("Average run time: " +
				avgTime.ToString() + "ms (" + 
				(avgTime / (float)1000).ToString() + "s)");
			writer.Close();
		}
		#endregion I/O functions

		#region accessors and mutators
		/// <summary>Returns the combined answers filename.</summary>
		public static string getOutputFileName()	{	return outputFileName;	}
		/// <summary>Sets the allAnswers boolean to modify output.</summary>
		public static void setAllAnswers()			{	allAnswers = true; }
		#endregion accessors and mutators
	}
}