﻿#region references
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
#endregion references

namespace Euler.Library
{
	class Swapper
	{
		#region swap functions
		public static void swap(ref string x, ref string y)
		{
			string temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref short x, ref short y)
		{
			short temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref int x, ref int y)
		{
			int temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref long x, ref long y)
		{
			long temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref float x, ref float y)
		{
			float temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref double x, ref double y)
		{
			double temp = x;
			x = y;
			y = temp;
		}
		public static void swap(ref BigInteger x, ref BigInteger y)
		{
			BigInteger temp = x;
			x = y;
			y = temp;
		}
		#endregion swap functions
	}
}