﻿#region hidden
#region references
using System;
using System.Reflection;
using System.Linq;
using System.Diagnostics;
using Euler.Problems;
using Euler.Library;
#endregion references

namespace Euler
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
#endregion hidden
			Problem prob = new Problem54();
			#region to-do list
			//Problem 26 - does not filter out complex repetition in long division.
			#endregion to-do list			
			#region hidden
		}

		#region private functions
		/// <summary>Uses reflection to call all subclasses' constructors, which runs through every problem.  A text file is created with every problem's answer and execution time and displayed after all problems are finished.</summary>
		private static void runAll()
		{
			Problem.deleteOutputFile();
			Problem.setAllAnswers();
			Problem p;
			foreach (Type t in Assembly.GetEntryAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(Problem))))
				p = (Problem)Activator.CreateInstance(t);
			Problem.sortOutFile();
			Process.Start(Problem.getOutputFileName());
			//Problem prob;
			//foreach (Type t in Assembly.GetAssembly(typeof(Problem)).GetTypes().Where(p => p.IsClass && !p.IsAbstract && p.IsSubclassOf(typeof(Problem))))
			//	prob = (Problem)Activator.CreateInstance(t);
		}
		#endregion private functions
	}
}
			#endregion hidden