﻿namespace Euler.Other
{
	partial class frmResults
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblResult = new System.Windows.Forms.Label();
			this.txtResult = new System.Windows.Forms.TextBox();
			this.lblTime = new System.Windows.Forms.Label();
			this.txtComments = new System.Windows.Forms.RichTextBox();
			this.lblComments = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblResult
			// 
			this.lblResult.AutoSize = true;
			this.lblResult.Location = new System.Drawing.Point(35, 21);
			this.lblResult.Name = "lblResult";
			this.lblResult.Size = new System.Drawing.Size(46, 13);
			this.lblResult.TabIndex = 0;
			this.lblResult.Text = "Result : ";
			// 
			// txtResult
			// 
			this.txtResult.BackColor = System.Drawing.Color.Black;
			this.txtResult.ForeColor = System.Drawing.Color.White;
			this.txtResult.Location = new System.Drawing.Point(87, 18);
			this.txtResult.Name = "txtResult";
			this.txtResult.Size = new System.Drawing.Size(208, 20);
			this.txtResult.TabIndex = 1;
			// 
			// lblTime
			// 
			this.lblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.lblTime.AutoSize = true;
			this.lblTime.Location = new System.Drawing.Point(146, 52);
			this.lblTime.Name = "lblTime";
			this.lblTime.Size = new System.Drawing.Size(39, 13);
			this.lblTime.TabIndex = 2;
			this.lblTime.Text = "Time : ";
			// 
			// txtComments
			// 
			this.txtComments.BackColor = System.Drawing.Color.Black;
			this.txtComments.ForeColor = System.Drawing.Color.White;
			this.txtComments.Location = new System.Drawing.Point(87, 79);
			this.txtComments.Name = "txtComments";
			this.txtComments.Size = new System.Drawing.Size(208, 146);
			this.txtComments.TabIndex = 3;
			this.txtComments.Text = "";
			// 
			// lblComments
			// 
			this.lblComments.AutoSize = true;
			this.lblComments.Location = new System.Drawing.Point(16, 82);
			this.lblComments.Name = "lblComments";
			this.lblComments.Size = new System.Drawing.Size(65, 13);
			this.lblComments.TabIndex = 4;
			this.lblComments.Text = "Comments : ";
			// 
			// frmResults
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
			this.ClientSize = new System.Drawing.Size(311, 243);
			this.Controls.Add(this.lblComments);
			this.Controls.Add(this.txtComments);
			this.Controls.Add(this.lblTime);
			this.Controls.Add(this.txtResult);
			this.Controls.Add(this.lblResult);
			this.ForeColor = System.Drawing.Color.White;
			this.Name = "frmResults";
			this.Text = "Project Euler - ";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblResult;
		private System.Windows.Forms.TextBox txtResult;
		private System.Windows.Forms.Label lblTime;
		private System.Windows.Forms.RichTextBox txtComments;
		private System.Windows.Forms.Label lblComments;
	}
}