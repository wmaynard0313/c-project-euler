﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
---------------------------------------------------------------------------------------------------
Answer:			233168
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem1 : Problem
	{
		#region variables
		private int sum = 0;
		#endregion variables

		#region calculations
		public Problem1()
		{
			init();

			for (int index = 0; index < 1000; index++)
				if (index % 3 == 0 || index % 5 == 0)
					sum += index;
			output(sum);
		}
		#endregion calculations
	}
}