﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.  

Find the sum of all the primes below two million.
---------------------------------------------------------------------------------------------------
Answer:			142913828922
Execution Time:	18 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem10 : Problem
	{
		#region variables
		private long sum = 0;
		#endregion variables

		#region calculations
		public Problem10()
		{
			init();

			foreach (int prime in EulerMath.getPrimes(max: 2000000))
				sum += prime;
			output(sum);
		}
		#endregion calculations
	}
}