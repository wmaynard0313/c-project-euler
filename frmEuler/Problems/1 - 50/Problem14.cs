﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The following iterative sequence is defined for the set of positive integers:

	n -> n/2 (n is even)
	n -> 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

	13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although
it has not been proven yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
---------------------------------------------------------------------------------------------------
Answer:			837799
Execution Time:	327 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem14 : Problem
	{
		#region variables
		private int[] map = new int[1000000];
		#endregion variables

		#region calculations
		public Problem14()
		{
			init();

			output(fillMap(ref map));
		}
		#endregion calculations

		#region private functions
		/// <summary>Fills the map with number of steps it takes to get to 1.  Returns the top result.</summary>
		private int fillMap(ref int[] map, int result = 0)
		{
			for (int index = 0; index < map.Length; index++)
			{
				map[index] = work(index, ref map);	// Save this count to the map for faster calculations.
				if (map[result] < map[index])
					result = index;
			}
			return result;
		}

		/// <summary>Filters the numbers down recursively.  Returns the number of steps required to get to 1.</summary>
		private int work(long input, ref int[] map, int count = 0)
		{
			// If input is over a million, maps[input] will err, but if maps[input] has a value, return it.
			if (input < map.Length && map[input] > 0)
				return map[input];
			// No value detected for maps[input].  Reduce the number further if not at 1.
			else if (input > 1)
				if (input % 2 == 0)
					count = work(input / 2, ref map);
				else
					count = work((input * 3) + 1, ref map);
			return ++count;
		}
		#endregion private functions
	}
}