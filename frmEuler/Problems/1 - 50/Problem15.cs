﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Starting in the top left corner of a 2x2 grid, there are 6 routes (without backtracking) to the
bottom right corner.

	(See Problem 15 - graph.gif)

How many routes are there through a 20x20 grid?
---------------------------------------------------------------------------------------------------
Answer:			137846528820
Execution Time:	232 ms
*/

#region manual solution
/*	Manual Solution
---------------------------------------------------------------------------------------------------
This problem can also be solved with Discrete Mathematics using the formula n!/(r!(n-r)!.
r is the size of the grid.
n is the number of segments to the end of the grid.

n = 2r (two segments per square).

This leaves us with:
(2 * 20)! / (20! * ((2*20) - 20)!)
= 40! / (20!)^2
= 40 * 39 * 38 * 37 * 36 * 35 * 34 * 33 * 32 * 31 * 30 * 29 * 28 * 27 * 26 * 25 * 24 * 23 * 22 * 21
/ 20 / 19 / 18 / 17 / 16 / 15 / 14 / 13 / 12 / 11 / 10 /  9 /  8 /  7 /  6 /  5 /  4 /  3 /  2 /  1

40, 39, and 35 are cancelled out with 2 and 20, 3 and 13, and 5 and 7 respectively.
= 38 * 37 * 36 * 34 * 33 * 32 * 31 * 30 * 29 * 28 * 27 * 26 * 25 * 24 * 23 * 22 * 21
/ 19 / 18 / 17 / 16 / 15 / 14 / 12 / 11 / 10 /  9 /  8 /  6 /  4

38, 36, 34, and 30 are reduced to 2 by 19, 18, 17, and 15 respectively.
=  2 * 37 *  2 *  2 * 33 * 32 * 31 *  2 * 29 * 28 * 27 * 26 * 25 * 24 * 23 * 22 * 21
/ 16 / 14 / 12 / 11 / 10 /  9 /  8 /  6 /  4

The four twos from the last steps cancel out 16.
= 37 * 33 * 32 * 31 * 29 * 28 * 27 * 26 * 25 * 24 * 23 * 22 * 21
/ 14 / 12 / 11 / 10 /  9 /  8 /  6 /  4

28, 24, and 22 are reduced to 2 by 14, 12, and 11 respectively.
= 37 * 33 * 32 * 31 * 29 * 2 * 27 * 26 * 25 * 2 * 23 * 2 * 21 / 10 / 9 / 8 / 6 / 4
 
These twos cancel out the 8.
= 37 * 33 * 32 * 31 * 29 * 27 * 26 * 25 * 23 * 21 / 10 / 9 / 6 / 4

32 reduced to 8 by 4.  27 is reduced to 3 by 9.
= 37 * 33 * 8 * 31 * 29 * 3 * 26 * 25 * 23 * 21 / 10 / 6
= 37 * 33 * 31 * 29 * 26 * 25 * 23 * 21 * 8 * 3 / 10 / 6

The rest requires further simplification.
= 37 * 33 * 31 * 29 * ((26 * 25) / 10 ) * 23 * 21 * ((8 * 3) / 6)
= 37 * 33 * 31 * 29 * (650 / 10) * 23 * 21 * (24 / 6)
= 37 * 33 * 31 * 29 * 65 * 23 * 21 * 4

Only multiplication is left.
= (37 * 33) * (31 * 29) * (65 * 4) * (23 * 21)
= 1221 * 899 * 260 * 483
= (1221 * 260) * (899 * 483)
= 317460 * 434217
= 137846528820
---------------------------------------------------------------------------------------------------
Answer: 137846528820
*/
#endregion manual solution
#endregion problem information

#region references
using System;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem15 : Problem
	{
		#region variables
		private const int size = 21;
		// Grid to be weighed by number of combinations.
		// Note: the grid corresponds to points, not cells, of the grid pictured,
		// which is why the size is one larger.
		private long[,] grid = new long[size, size];
		#endregion variables

		#region calculations
		public Problem15()
		{ // 137846528820 : 106ms
			init();

			// Move through each row.
			for (int row = 0; row < size; row++)
				// Move through columns starting at a square, as we only need half of the grid.
				for (int col = row; col < size; col++)
					if (col > 0 && col == row)
						grid[row, col] = grid[row - 1, col] * 2;
					else
						try { grid[row, col] = grid[row - 1, col] + grid[row, col - 1]; }
						catch (Exception) { grid[row, col] = 1; }	// Fill top row with 1.
			output(grid[size - 1, size - 1]);
		}
		#endregion calculations
	}
}