﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2^1000?
---------------------------------------------------------------------------------------------------
Answer:			1366
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem16 : Problem
	{
		#region variables
		private int result = 0;
		#endregion variables

		#region calculations
		public Problem16()
		{
			init();

			foreach (char c in EulerBigInt.pow("2", 1000))
				result += int.Parse(c.ToString());
			output(result);
		}
		#endregion calculations
	}
}