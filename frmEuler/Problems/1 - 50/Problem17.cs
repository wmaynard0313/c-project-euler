﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are:
	
	3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many 
letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 
letters and 115 (one hundred and fifteen) contains 20 letters. The use of 'and' when writing out
numbers is in compliance with British usage.
---------------------------------------------------------------------------------------------------
Answer:			21124
Execution Time:	3 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem17 : Problem
	{
		#region variables
		private int charCount = 0;
		// Verbose numbers.
		private List<string> vNums = new List<string>();
		// Verbose strings of numbers.
		private string[] singles = new string[] {
			"One", "Two", "Three", 
			"Four", "Five", "Six", 
			"Seven", "Eight", "Nine" };
		private string[] tenToNineteen = new string[] {
			"", "Eleven", "Twelve", "Thirteen", 
			"Fourteen", "Fifteen", "Sixteen", 
			"Seventeen", "Eighteen", "Nineteen" };
		private string[] doubles = new string[] {
			"Ten", "Twenty", "Thirty", 
			"Forty", "Fifty", "Sixty", 
			"Seventy", "Eighty", "Ninety" };
		private string hundreds = "Hundred";
		private string thousands = "Thousand";
		private string and = "And";
		#endregion variables

		#region calculations
		public Problem17()
		{
			init();

			// Type out every number.
			for (int index = 1; index <= 1000; index++)
				type(index, ref vNums);

			// Remove all hyphens and whitespace, sum all of the lengths up.
			for (int index = 0; index < vNums.Count; index++)
			{
				vNums[index] = vNums[index].Replace(" ", "");
				vNums[index] = vNums[index].Replace("-", "");
				charCount += vNums[index].Length;
			}
			output(charCount);
		}
		#endregion calculations

		#region private functions
		/// <summary>Returns the first digit of a number.</summary>
		private int firstDigit(int input)
		{
			return input / (int)(Math.Pow(10, input.ToString().Length - 1));
		}
		/// <summary>Types out a number according to British usage.</summary>
		private void type(int num, ref List<string> results)
		{
			int temp = num;
			bool hundred = false;
			bool ten = false;
			bool overtwenty = false;
			bool teen = false;
			string verbose = "";

			if (num >= 1000)	// Add thousands prefix.
			{
				verbose += singles[firstDigit(num) - 1] + ' ' + thousands;
				num %= 1000;
			}
			if (num >= 100)		// Add hundreds prefix.
			{
				hundred = true;
				verbose += singles[firstDigit(num) - 1] + ' ' + hundreds;
				num %= 100;
			}
			if (num >= 10)		// Add tens.
			{
				ten = true;
				if (hundred)
					verbose += ' ' + and + ' ';
				if (num < 20 && num > 10)	// Special spelling for teens.
				{
					teen = true;
					verbose += tenToNineteen[firstDigit(num % 10)];
				}
				else						// Standard spelling.
				{
					overtwenty = true;
					verbose += doubles[firstDigit(num) - 1];
				}
				num %= 10;
			}
			if (num > 0 && !teen)	// Only teens account for the last digit.
			{
				if (hundred && !ten)	// e.g. 106, 201
					verbose += ' ' + and + ' ';
				if (overtwenty)		// Add a dash.  e.g. forty-five.
					verbose += '-';
				verbose += singles[firstDigit(num) - 1];
			}
			vNums.Add(verbose);
		}
		#endregion private functions
	}
}