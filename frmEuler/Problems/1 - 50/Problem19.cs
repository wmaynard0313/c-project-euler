﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
You are given the following information, but you may prefer to do some research for yourself.

	-	1 Jan 1900 was a Monday.

	-	Thirty days has September,
		April, June and November.
		All the rest have thirty-one,
		Saving February alone,
		Which has twenty-eight, rain or shine.
		And on leap years, twenty-nine.

	-	A leap year occurs on any year evenly divisible by 4, but not on a century unless it is
divisible by 400.

How many Sundays fell on the first of the month during the twentieth century 
(1 Jan 1901 to 31 Dec 2000)?
---------------------------------------------------------------------------------------------------
Answer:			171
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem19 : Problem
	{
		#region variables
		private int count = 0;
		private int currentDay = 2;  // 1 January 1901 was a Tuesday.  Zero-indexed.  Sunday is 0.
		// Lengths of each month.
		private int[] lengths = new int[] {31, 28, 31, 30,31, 30, 31, 31, 30, 31, 30, 31 };
		#endregion variables

		#region calculations
		public Problem19()
		{
			init();

			// Move through each year.
			for (int year = 1901; year < 2001; year++)
			{
				// If a leap year, add a day to February.
				if (year % 4 == 0)
					lengths[1] = 29;
				else
					lengths[1] = 28;

				// Add each month's number of days to current day to get the first of the next month.
				for (int index = 0; index < lengths.Length; currentDay += lengths[index++])
					if (currentDay % 7 == 0)
						count++;
			}
			output(count);
		}
		#endregion calculations
	}
}