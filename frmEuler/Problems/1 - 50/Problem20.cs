﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
n! means n x (n - 1)  ...  3 x 2 x 1

	For example, 10! = 10 x 9  ...  3 x 2 x 1 = 3628800,
	and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
---------------------------------------------------------------------------------------------------
Answer:			648
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem20 : Problem
	{
		#region variables
		private int digitSum = 0;
		private string factorial = EulerBigInt.factorial("100");
		#endregion variables

		#region calculations
		public Problem20()
		{
			init();

			//Add all the digits of the factorial together.
			for (int index = 0; index < factorial.Length; index++)
				digitSum += EulerMath.charToInt(factorial[index]);
			output(digitSum);
		}
		#endregion calculations
	}
}