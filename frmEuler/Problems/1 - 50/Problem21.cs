﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly
into n).

If d(a) = b and d(b) = a, where a  b, then a and b are an amicable pair and each of a and b are
called amicable numbers.

For example: 
	The proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore
		d(220) = 284. 
	The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
---------------------------------------------------------------------------------------------------
Answer:			31626
Execution Time:	45 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem21 : Problem
	{
		#region variables
		private List<long> pairs = new List<long>();
		#endregion variables

		#region calculations
		public Problem21()
		{
			init();

			// Move up through every number.
			for (int index = 0; index < 10000; index++)
				// Skip calculations if the result is already in the list.
				if (!pairs.Contains(index))
				{
					int sum = EulerMath.sum(EulerMath.factor(index)) - index;
					int sum2 = EulerMath.sum(EulerMath.factor(sum)) - sum;

					// Sum is not the original number, and the second sum is.
					if (sum2 == index && sum != index)
					{
						pairs.Add(sum);
						pairs.Add(index);
					}
				}
			output(EulerMath.sum(pairs));
		}
		#endregion variables
	}
}