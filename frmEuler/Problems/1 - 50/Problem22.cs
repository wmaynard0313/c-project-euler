﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Using names.txt, a 46K text file containing over five-thousand first names, begin by sorting it
into alphabetical order. Then working out the alphabetical value for each name, multiply this value
by its alphabetical position in the list to obtain a name score.

	For example, when the list is sorted into alphabetical order, COLIN is worth:
		3 + 15 + 12 + 9 + 14 = 53
	COLIN is the 938th name in the list. So, COLIN would obtain a score of 938 x 53 = 49714.

What is the total of all the name scores in the file?
---------------------------------------------------------------------------------------------------
Answer:			871198282
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem22 : Problem
	{
		#region variables
		private int result = 0;
		private string[] names = EulerString.separate("Problem 22 - names.txt");
		#endregion variables

		#region calculations
		public Problem22()
		{
			init();

			// Calculate the score of each name and sum it up.
			for (int index = 0; index < names.Length; index++)
				result += EulerString.sumCharValues(names[index]) * (index + 1);
			output(result);
		}
		#endregion calculations
	}
}