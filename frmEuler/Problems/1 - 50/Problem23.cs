﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
A perfect number is a number for which the sum of its proper divisors is exactly equal to the
number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which
means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called
abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be
written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that
all integers greater than 28123 can be written as the sum of two abundant numbers. However, this
upper limit cannot be reduced any further by analysis even though it is known that the greatest
number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant
numbers.
---------------------------------------------------------------------------------------------------
Answer:			4179871
Execution Time:	654 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem23 : Problem
	{
		#region variables
		private bool[] sums = new bool[28124];
		private long result = 0;
		private List<int> abundants = new List<int>();
		#endregion variables

		#region calculations
		public Problem23()
		{
			init();

			// Find all abundant numbers.
			for (int index = 1; index <= 28123; index++)
				if (index < EulerMath.sum(EulerMath.factor(index)) - index)
					abundants.Add(index);

			// Find all sums of two abundant numbers' factors which are less than 28123.
			for (int outdex = 0; outdex < abundants.Count; outdex++)
				for (int index = 0; index < abundants.Count; index++)
				{
					int sum = abundants[outdex] + abundants[index];
					if (sum <= 28123)
						sums[sum] = true;
				}

			// Sum all of the numbers that cannot be written as the sum of two abundant numbers.
			for (int index = 1; index <= 28123; index++)
				if (!sums[index])
					result += index;
			output(result);
		}
		#endregion calculations
	}
}