﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation
of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically,
we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

	012 , 021 , 102 , 120 , 201 , 210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
---------------------------------------------------------------------------------------------------
Answer:			2783915460
Execution Time:	0 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem24 : Problem
	{
		#region variables
		private int termIndex = 999999; // Position of element we want to find.
		private string nums = "0123456789";
		private string result = "";
		#endregion variables

		#region calculations
		public Problem24()
		{
			init();

			//Each new digit has X! possibilities.  Start high and move down.
			for (int index = 9; index > 0; index--)
			{
				int fact = (int)EulerMath.factorial(index); // Maximum value of current digit.
				int divisor = termIndex / fact;				// How many times digit is needed.
				termIndex -= fact * divisor;				// Remaining index to find.
	
				result += nums[divisor];					// Add the required digit.
				nums = nums.Remove(divisor, 1);				// Each digit may only be used once.
			}
			result += nums[0];								// Add the last digit.
			output(result);
		}
		#endregion calculations
	}
}