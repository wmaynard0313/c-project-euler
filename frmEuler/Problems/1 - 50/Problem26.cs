﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with
denominators 2 to 10 are given:

	1/2		= 	0.5
	1/3		= 	0.(3)
	1/4		= 	0.25
	1/5		= 	0.2
	1/6		= 	0.1(6)
	1/7		= 	0.(142857)
	1/8		= 	0.125
	1/9		= 	0.(1)
	1/10	= 	0.1

	Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has
	a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal
fraction part.
---------------------------------------------------------------------------------------------------
Answer:			983
Execution Time:	24 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem26 : Problem
	{
		#region variables
		private int[] longest = new int[] { 0, 0 };
		private List<int> primes = EulerMath.getPrimes(max: 1000);
		#endregion variables

		#region calculations
		public Problem26()
		{
			init();

			// Prime numbers result in longer repetition sequences.
			for (int index = 2; index < primes.Count; index++)
			{
				string temp = new EulerBigDecimal(1, primes[index]).ToString();
				if (temp.Length > longest[1])
				{
					longest[0] = primes[index];
					longest[1] = temp.Length;
				}
			}
			output(longest[0]);
		}
		#endregion calculations
	}
}

// Still need to filter out repetition in approximations like 13/14.