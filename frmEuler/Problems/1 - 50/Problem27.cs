﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Euler published the remarkable quadratic formula:

	n^2 + n + 41

It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39.
However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when
n = 41, 41^2 + 41 + 41 is clearly divisible by 41.

Using computers, the incredible formula  n^2 - 79n + 1601 was discovered, which produces 80 primes
for the consecutive values n = 0 to 79. The product of the coefficients, 79 and 1601, is 126479.

Considering quadratics of the form:

	n^2 + an + b, where |a| < 1000 and |b| < 1000

	where |n| is the modulus/absolute value of n
	e.g. |11| = 11 and |4| = 4

Find the product of the coefficients, a and b, for the quadratic expression that produces the
maximum number of primes for consecutive values of n, starting with n = 0.
---------------------------------------------------------------------------------------------------
Answer:			-59231
Execution Time:	252 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem27 : Problem
	{
		#region variables
		private const int max = 1000;
		private int count = 0;
		private int mostPrimes = 0;
		private int product = 0;
		private List<int> primes = Library.EulerMath.getPrimes(max: max);
		#endregion variables

		#region calculations
		public Problem27()
		{
			init();
			
			// For each prime, add a negative prime.
			for (int index = 0, tempCount = primes.Count; index < tempCount; index++)
				primes.Add(primes[index] * (-1));
			primes.Sort();
			// Loop for the coefficient, a.
			for (int outdex = 0; outdex < primes.Count; outdex++)
				// Loop for the constant, b.
				for (int index = 0; index < primes.Count; index++, count = 0)
					// All 'n' values are prime.  More 'n' values than previous combination of a and b.
					if (allPrime(primes[outdex], primes[index], ref count, primes) && mostPrimes < count)
					{
						mostPrimes = count;
						product = primes[index] * primes[outdex];
					}
			output(product);
		}
		#endregion calculations

		#region private functions
		/// <summary>Tests if all 'n' values (less than the coefficient and constant) are prime.</summary>
		private bool allPrime(int coefficient, int constant, ref int count, List<int> results)
		{
			// Count is the number of primes for consecutive values of 'n'.
			for (count = 0; count < abs(constant) && count < abs(coefficient); )
				// If the quadratic does not produce a prime.  Increase the 'n' value.
				if (!results.Contains(abs(func(count++, coefficient, constant))))
					return false;
			return true;
		}
		/// <summary>Returns the absolute value of a number.</summary>
		private int abs(int input)
		{
			if (input < 0)
				return (-1) * input;
			return input;
		}
		// Perform Euler's quadratic with specified parameters.
		private int func(int n, int coefficient, int constant)
		{
			return (n * n) + (coefficient * n) + constant;
		}
		#endregion private functions
	}
}