﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is
formed as follows:

	21 22 23 24 25
	20  7  8  9 10
	19  6  1  2 11
	18  5  4  3 12
	17 16 15 14 13

	It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
---------------------------------------------------------------------------------------------------
Answer:			669171001
Execution Time:	4615 ms
*/
#endregion problem information

#region references
using System;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem28 : Problem
	{
		#region variables
		private int max = 1001 * 1001;
		private int[] grid = { 1 };
		#endregion variables

		#region calculations
		public Problem28()
		{
			init();

			// Build the 1001 x 1001 grid.
			while (grid.Length < max)
			{
				expand(ref grid, right: true);
				expand(ref grid, down: true);
				expand(ref grid, left: true);
				expand(ref grid, up: true);
			}
			output(sumDiagonals(grid));
		}
		#endregion variables

		#region private functions
		/// <summary>Returns a sum of the diagonal entries of the grid.</summary>
		/// <summary>Expands the grid, a one-dimentional array which represents a two-dimensional one.  Only one boolean parameter is accepted at a time.</summary>
		private void expand(ref int[] grid, bool down = false, bool right = false, bool left = false, bool up = false)
		{
			int[] newGrid = new int[] { };

			int length = grid.Length;
			int sqrt = (int)Math.Sqrt(length);

			// Expand the grid as if adding a column to the right side of a 2D array.
			if (right)
			{
				newGrid = new int[length + sqrt];
				// Add the old elements to their respective places in the new array.
				for (int index = 0, oldIndex = 0; index < newGrid.Length && oldIndex < length; index++)
				{
					if (index % (sqrt + 1) == sqrt)
						index++;
					newGrid[index] = grid[oldIndex++];
				}
				// Fill empty cells with their appropriate numbers.
				for (int index = sqrt++; index < newGrid.Length; index += sqrt)
					if (index == sqrt - 1)
						newGrid[index] = newGrid[index - 1] + 1;
					else
						newGrid[index] = newGrid[index - sqrt] + 1;
			}
			// Expand the grid as if adding a column to the left side of a 2D array.
			else if (left)
			{
				newGrid = new int[length + sqrt];
				newGrid[0] = newGrid.Length;
				for (int index = 1, oldIndex = 0; index < newGrid.Length; index++)
					// Fill empty cells with their appropriate numbers.
					if (index % (sqrt + 1) == 0)
						newGrid[index] = newGrid[index - (sqrt + 1)] - 1;
					// Add the old elements to their respective places in the new array.
					else
						newGrid[index] = grid[oldIndex++];
			}
			// Expand the grid as if adding a row to the bottom side of a 2D array.
			else if (down)
			{
				newGrid = new int[length + sqrt + 1];
				// Add the old elements to their respective places in the new array.
				for (int oldIndex = 0; oldIndex < grid.Length; oldIndex++)
					newGrid[oldIndex] = grid[oldIndex];
				// Fill empty cells with their appropriate numbers.
				newGrid[newGrid.Length - 1] = grid[length - 1] + 1;
				for (int index = newGrid.Length - 2; !(newGrid[index] > 0); index--)
					newGrid[index] = newGrid[index + 1] + 1;
			}
			// Expand the grid as if adding a row to the top side of a 2D array.
			else if (up)
			{
				newGrid = new int[length + sqrt + 1];
				int temp = grid.Length + 1;

				for (int index = 0, oldIndex = 0; index < newGrid.Length; index++)
					// Fill empty cells with their appropriate numbers.
					if (index < newGrid.Length - length)
						newGrid[index] = temp++;
					// Add the old elements to their respective places in the new array.
					else
						newGrid[index] = grid[oldIndex++];
			}
			grid = newGrid;
		}
		/// <summary>Sum all diagonally adjacent corners in an X pattern over a square represented by a 1-Dimensional array..</summary>
		private int sumDiagonals(int[] grid)
		{
			int sqrt = (int)Math.Sqrt(grid.Length);

			// Picture two separate lines starting from the top corners, moving down to the center,
			// then moving to the bottom corners of the same side they started on.  Variables "first"
			// and "second" are tracking these lines on a one-dimensional array.

			int first = 0;			// First entry on each row to add.
			int second = sqrt - 1;	// Second entry on each row to add.
			int sum = 0;			// Sum of all diagonal spaces.
			int index = 0;			// Index for entry to add.

			// Move down to the center of the grid.
			while (first < sqrt && second > 0)
			{
				sum += grid[index];	// Add first entry.
				index += second;	// Get index of next entry.
				first += 2;			// Add 2 to get the index of next row's first entry.
				
				sum += grid[index];	// Add second entry.
				index += first;		// Get index of next entry.
				second -= 2;		// Subtract 2 to get the index of next row's second entry.
			}

			sum += grid[index];		// Add the center entry.

			// Reinitialize the variables for reversing the direction of first / second entries.
			first = sqrt - 1;
			second = 2;
			index += first;

			// Reverse the process to move down to the bottom of the grid.
			while (second < sqrt && first > 0)
			{
				sum += grid[index];	// Add first entry.
				index += second;	// Get index of next entry.
				first -= 2;			// Subtract 2 to get the index of next row's second entry.

				sum += grid[index];	// Add second entry.
				index += first;		// Get index of next entry.
				second += 2;		// Add 2 to get the index of next row's second entry.
			}

			return sum;
		}		
		#endregion private functions
	}
}