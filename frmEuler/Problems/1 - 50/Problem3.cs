﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143?
---------------------------------------------------------------------------------------------------
Answer:			6857
Execution Time:	0 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem3 : Problem
	{
		#region variables
		private List<long> list = EulerMath.primeFactor(600851475143);
		#endregion variables

		#region calculations
		public Problem3()
		{
			init();

			output(list[list.Count - 1]);
		}
		#endregion calculations
	}
}