﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Surprisingly there are only three numbers that can be written as the sum of fourth powers of their
digits:

	1634 = 1^4 + 6^4 + 3^4 + 4^4
	8208 = 8^4 + 2^4 + 0^4 + 8^4
	9474 = 9^4 + 4^4 + 7^4 + 4^4
	
	As 1 = 1^4 is not a sum it is not included.

	The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
---------------------------------------------------------------------------------------------------
Answer:			443839
Execution Time:	757 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem30 : Problem
	{
		#region variables
		private List<int> results = new List<int>();
		#endregion variables

		#region calculations
		public Problem30()
		{
			init();

			// Move through every number.
			for (int outdex = 2, max = findMaxNum(), digitSum = 0; outdex <= max; outdex++, digitSum = 0)
			{
				// Sum all of the fifth powers of each digit.
				for (int index = 0; index < outdex.ToString().Length; index++)
					digitSum += (int)Math.Pow(int.Parse(outdex.ToString()[index].ToString()), 5);
				// If the sum is equal to the original number, add it to results.
				if (digitSum == outdex)
					results.Add(outdex);
			}
			output(EulerMath.sum(results));
		}
		#endregion calculations

		#region private functions
		/// <summary>Discovers when adding a digit results in more gain than adding the fifth power of a new digit.</summary>
		private int findMaxNum()
		{
			string maxString = "9";
			int pow = 0;

			while (true)
			{
				pow += (int)Math.Pow(9, 5);
				maxString += '9';

				if (int.Parse(maxString) > pow)
					break;
			}
			return pow;
		}
		#endregion private functions
	}
}