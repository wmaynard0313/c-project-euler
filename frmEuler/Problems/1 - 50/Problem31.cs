﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
In England the currency is made up of pound, £, and pence, p, and there are eight coins in general
circulation:

	1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
				
	It is possible to make £2 in the following way:

	1£1 + 150p + 220p + 15p + 12p + 31p

How many different ways can £2 be made using any number of coins?
---------------------------------------------------------------------------------------------------
Answer:			73682
Execution Time:	123 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem31 : Problem
	{
		#region variables
		private const int two = 2;
		private const int five = 5;
		private const int ten = 10;
		private const int twenty = 20;
		private const int fifty = 50;
		private const int onehundred = 100;
		private const int twohundred = 200;
		private const int max = 200;
		private int count = 0;
		#endregion variables

		#region calculations
		public Problem31()
		{
			init();

			// Starting with the smallest denomination, use a loop
			// to count all different ways to add up to 200.
			for (int TH = 0; TH <= max / twohundred; TH++)
				for (int OH = 0; OH <= max / onehundred; OH++)
					for (int FIF = 0; FIF <= max / fifty; FIF++)
						for (int TWE = 0; TWE <= max / twenty; TWE++)
							for (int TE = 0; TE <= max / ten; TE++)
								for (int FIV = 0; FIV <= max / five; FIV++)
									for (int TWO = 0; TWO <= max / two; TWO++)
											if (
												TH * twohundred +
												OH * onehundred +
												FIF * fifty +
												TWE * twenty +
												TE * ten +
												FIV * five +
												TWO * two
												<= max)
												count++;
			output(count);
		}
		#endregion calculations
	}
}