﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly
once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 * 186 = 7254, containing multiplicand, multiplier,
and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a
1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your
sum.
---------------------------------------------------------------------------------------------------
Answer:			45228
Execution Time:	22 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using System.Linq;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem32 : Problem
	{
		#region variables
		private bool[] bnums = new bool[5000];
		private List<int> results = new List<int>();
		#endregion variables

		#region calculations
		public Problem32()
		{
			init();

			// Find all potential multiplicands and multipliers.
			for (int index = 0; index <= 4999; index++)
				if (!EulerString.repeatsChars(index.ToString()) && !index.ToString().Contains('0'))
					bnums[index] = true;
			// Move up through all the multiplicands.
			for (int outdex = 0; outdex < bnums.Length; outdex++)
				if (bnums[outdex])
					// Move up through all the multipliers.
					for (int index = outdex + 1; index < bnums.Length; index++)
						if (bnums[index])
							// Make sure no numbers have already been repeated.
							if (!EulerString.repeatsChars(String.Concat(outdex, index)))
							{
								int prod = outdex * index;
								// If result is pandigital and is not already listed, add it.
								if (EulerMath.isPandigital(String.Concat(outdex, index, prod)))
									if (!results.Contains(prod))
										results.Add(prod);
							}
							// If the concatenation is over 9 digits, any further multipliers will not work.
							else if (String.Concat(outdex, index, outdex * index).Length > 9)
								break;
			output(EulerMath.sum(results));
		}
		#endregion calculations
	}
}