﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to
simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling
the 9s.

	We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and
containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the
denominator.
---------------------------------------------------------------------------------------------------
Answer:			100
Execution Time:	4 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem33 : Problem
	{
		#region variables
		private float denproduct = 1;
		private float numproduct = 1;
		private List<float> numbers = new List<float>();
		#endregion variables

		#region calculations
		public Problem33()
		{
			init();		

			// Move up through all the possible numerators up to 99.
			for (float numerator = 10; numerator < 100; numerator++)
				// If numerator is not a multiple of 10 and not double digits...
				if (numerator % 10 != 0 && !isDoubleDigits(numerator))
					// Move down through denominators to numerator, fraction must be < 1.
					for (float denominator = 99; denominator > numerator; denominator--)
						// If denominator is not a multiple of 10 and not double digits...
						if (denominator % 10 != 0 && !isDoubleDigits(denominator))
						{
							int sim = findSameDigit(numerator, denominator);
							if (sim != 0)
							{
								// Remove the shared digit from both the numerator and denominator.
								float x = float.Parse(numerator.ToString().Replace(sim.ToString(), ""));
								float y = float.Parse(denominator.ToString().Replace(sim.ToString(), ""));

								// If the proportions are the same, add them to the number list.
								if (x / y == numerator / denominator)
								{
									numbers.Add(numerator);
									numbers.Add(denominator);
								}
							}
						}
			// Multiply all the numerators together, then all the denominators.
			for (int index = 0; index < numbers.Count; index++)
			{
				numproduct *= numbers[index++];
				denproduct *= numbers[index];
			}
			// Simplify the fraction of numproduct / denproduct.
			for (int index = 2; index <= numproduct; index++)
				if (numproduct % index == 0 && denproduct % index == 0)
				{
					numproduct /= index;
					denproduct /= index;
					index = 2;
				}
			output(denproduct);
		}
		#endregion calculations

		#region private functions
		/// <summary>Tests to see if the two digits in a number are identical.</summary>
		private bool isDoubleDigits(float input)	{	return input.ToString()[0] == input.ToString()[1];	}
		/// <summary>Finds the shared digit between two numbers.</summary>
		private int findSameDigit(float x, float y)
		{
			string numerator = x.ToString();
			string denominator = y.ToString();

			if (numerator[0] == denominator[0] || numerator[0] == denominator[1])
				return int.Parse(numerator[0].ToString());
			else if (numerator[1] == denominator[0] || numerator[1] == denominator[1])
				return int.Parse(numerator[1].ToString()); ;
			return 0;
		}
		#endregion private functions
	}
}