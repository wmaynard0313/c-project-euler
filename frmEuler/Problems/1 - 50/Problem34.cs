﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
---------------------------------------------------------------------------------------------------
Answer:			40730
Execution Time:	2699 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem34 : Problem
	{
		#region variables
		private int result = 0;
		private long max = 10;
		#endregion variables

		#region calculations
		public Problem34()
		{
			init();

			// Find the max value by figuring out when adding 10! is less of a difference than adding another digit.
			for (int t = 9; max >= t; t = t * 10 + 9)
			{
				max = 0;
				foreach (char c in t.ToString())
					max += EulerMath.factorial(int.Parse(c.ToString()));
			}

			// If the sum of factorials on each digit is equal to the index, add them to the result.
			for (int index = 3; index <= max; index++)
			{
				long fsum = 0;
				foreach (char c in index.ToString())
					// If the sum ever exceeds the number, stop.
					if ((fsum += EulerMath.factorial(int.Parse(c.ToString()))) > index)
						break;
				if (fsum == index)
					result += index;
			}
			output(result);
		}
		#endregion calculations
	}
}