﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719,
are themselves prime.

	There are thirteen such primes below 100: 
	2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
---------------------------------------------------------------------------------------------------
Answer:			55
Execution Time:	9794 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem35 : Problem
	{
		#region variables
		private bool allPrime = true;
		private int count = 0;
		private List<int> primes = EulerMath.getPrimes(max: 1000000);
		private Queue<int> addQueue = new Queue<int>();
		#endregion variables

		#region calculations
		public Problem35()
		{
			init();			

			for (int index = 0; index < primes.Count; allPrime = true, addQueue.Clear())
			{
				string prime = primes[index].ToString();		// Current prime in string format.
				addQueue.Enqueue(extract(ref primes, index));	// Takes out the prime at index.

				if (int.Parse(prime) > 11)	// 11 is a special case as the digits are identical.
					// Move the digits around, testing if each combination is prime.
					for (int i = 0; i < prime.Length - 1 && allPrime; i++)
					{
						prime = prime.Substring(1) + prime[0];
						int iprime = int.Parse(prime);

						// If the rotated number is also prime, add it to the queue.
						if (primes.Contains(iprime))
							addQueue.Enqueue(extract(ref primes, primes.IndexOf(iprime)));
						else
							allPrime = false;
					}
				else
					allPrime = true;
				// If all numbers were prime, increment the counter.
				if (allPrime)
					count += addQueue.Count;
			}
			output(count);
		}
		#endregion calculations

		#region private functions
		/// <summary>Removes an item from a list and returns it.</summary>
		private static int extract(ref List<int> input, int index)
		{
			int temp = input[index];
			input.RemoveAt(index);
			return temp;
		}
		#endregion private functions
	}
}