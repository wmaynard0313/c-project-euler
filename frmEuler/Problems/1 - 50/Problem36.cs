﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The decimal number, 585 = 1001001001(b) , is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

Please note that the palindromic number, in either base, may not include leading zeros.
---------------------------------------------------------------------------------------------------
Answer:			872187
Execution Time:	0 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem36 : Problem
	{
		#region variables
		private int sum = 0;
		private List<int> palindromes = EulerMath.getPalindromes(1000000);
		private List<string> binaries = new List<string>();
		#endregion variables

		#region calculations
		public Problem36()
		{
			init();

			// Convert each palindrome to binary.
			foreach (int i in palindromes)
				binaries.Add(EulerMath.decToBinary(i));
			// If the binary is also a palindrome, add the number to the sum.
			for (int index = 0; index < binaries.Count; index++)
				if (EulerString.isPalindrome(binaries[index]))
					sum += palindromes[index];
			output(sum);
		}
		#endregion calculations
	}
}