﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The number 3797 has an interesting property. Being prime itself, it is possible to continuously 
remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly 
we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to 
left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
---------------------------------------------------------------------------------------------------
Answer:			748317
Execution Time:	26714 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem37 : Problem
	{
		#region variables
		private List<int> primes = new List<int>();
		private List<int> trunks = new List<int>();
		#endregion variables

		#region calculations
		public Problem37()
		{
			init();

			while (trunks.Count < 11)
			{
				EulerMath.addNextPrime(ref primes);
				if (EulerMath.isTruncatable(primes[primes.Count - 1], primes))
					trunks.Add(primes[primes.Count - 1]);
			}
			output(EulerMath.sum(trunks));
		}
		#endregion calculations
	}
}