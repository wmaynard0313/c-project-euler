﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Take the number 192 and multiply it by each of 1, 2, and 3:

	192 * 1 = 192
	192 * 2 = 384
	192 * 3 = 576

	By concatenating each product we get the 1 to 9 pandigital, 192384576. 
	We will call 192384576 the concatenated product of 192 and (1,2,3).

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the 
pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product
of an integer with (1,2, ... , n) where n > 1?
---------------------------------------------------------------------------------------------------
Answer:			932718654
Execution Time:	4
*/
#region manual solution
/*
Manual Solution:
---------------------------------------------------------------------------------------------------
The highest possible pandigital number is 987654321: F(987654321, (1)).
 
Problem 38 states that we must have at least two multiplicants.
 
Since no multiplicant will affect another multiplicant's product (they are concatenated),
we can conclude that the more multiplicants there are, 
the smaller the result will be (more restrictions).

Thus, the optimal number of multiplicants is 2.

Find F(x, (1, 2)).

Comment Key:
# - #:
	Numbers being commented on.
D(M, #):
	M - multiplicant
	# - number that would be duplicated.
Z(M):
	M - multiplicant that would result in a 0 with the number.
	  
|	x1	|	x2	|	Remaining	|	Comments	
|*******|*******|***************|*****************************************
| 9		| 18	|	234567		|	5 - 7: D(9),	4: D(8)
|		|		|				|	A duplicate 8 can't be fixed on x2 as it can only go up to a 9.
|		|		|	23			|
| 93	| 186	|	2457		|	7: D(7),	5: Z(2),	4: D(8)
|		|		|	2			|
| 932	| 1864	|	7			|
| 9327	| 18654	|				|
			 
Answer: 932718654, F(9327, (1, 2))
*/
#endregion manual solution

#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem38 : Problem
	{
		#region variables
		private int highest = 0; 
		private string result = "";
		#endregion variables

		#region calculations
		public Problem38()
		{
			init();

			for (int outdex = 1; outdex < 10000; outdex++, result = "")
			{
				// Concatenate result with new products until length of 9 is reached.
				for (int index = 1; result.Length < 9; index++)
					result += (outdex * index).ToString();

				// If result has 9 digits, is pandigital, and is higher than the current record, track it.
				if (result.Length == 9)
				{
					int iresult = int.Parse(result);
					if (highest < iresult && EulerMath.isPandigital(result))
						highest = iresult;
				}
			}
			output(highest);
		}
		#endregion calculations
	}
}