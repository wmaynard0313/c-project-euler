﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are 
exactly three solutions for p = 120.

	{20,48,52}, {24,45,51}, {30,40,50}

For which value of p <= 1000, is the number of solutions maximised?
---------------------------------------------------------------------------------------------------
Answer:			840
Execution Time:	1550 ms
*/
#endregion problem information

#region references
using System;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem39 : Problem
	{
		#region variables
		private int[] max = new int[] { 0, 0 };	//[p][Solution Count]
		#endregion variables

		#region calculations
		public Problem39()
		{
			init();
			
			for (int p = 0, count = 0; p <= 1000; p++, count = 0)
			{
				for (int b = 1; b < p; b++)
					for (int a = 1; a < b; a++)
						if (a + b + Math.Sqrt(a * a + b * b) == p)
							count++;
				if (count > max[1])
				{
					max[1] = count;
					max[0] = p;
				}
			}
			output(max[0]);
		}
		#endregion calculations
	}
}