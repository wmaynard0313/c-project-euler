﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
A palindromic number reads the same both ways. The largest palindrome made from the product of two
2-digit numbers is 9009 = 91 x 99.  
			
Find the largest palindrome made from the product of two 3-digit numbers.
---------------------------------------------------------------------------------------------------
Answer:			906609
Execution Time:	61 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem4 : Problem
	{
		#region variables
		private int result = 0;
		#endregion variables

		#region calculations
		public Problem4()
		{
			init();

			// Move through each 3-digit number.
			for (int outdex = 100; outdex <= 999; outdex++)
				// Multiply it by every higher 3-digit number.
				for (int index = outdex, product; index <= 999; index++)
					if (EulerString.isPalindrome((product = index * outdex).ToString()))
						if (product > result)
							result = product;
			output(result);
		}
		#endregion calculations
	}
}