﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
An irrational decimal fraction is created by concatenating the positive integers:

	0.123456789101112131415161718192021...
	             
	It can be seen that the 12th digit of the fractional part is 1.

If d(n) represents the nth digit of the fractional part, find the value of the following
expression:

	d(1) x d(10) x d(100) x d(1000) x d(10000) x d(100000) x d(1000000)
---------------------------------------------------------------------------------------------------
Answer:			210
Execution Time:	0 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem40 : Problem
	{
		#region variables
		private int i = 1;
		private int product = 1;
		private int[] indices = new int[] { 1, 10, 100, 1000, 10000, 100000, 1000000 };
		private long max = 1000000;
		private List<long> ranges = new List<long>();
		#endregion variables

		#region calculations
		public Problem40()
		{
			init();
			
			// Each entry in ranges refers to how many different characters an i-digit number adds.
			do ranges.Add((long)(9 * Math.Pow(10, i - 1)) * i++);
			while (ranges[ranges.Count - 1] < max);

			// Find the character pointed to by each entry in indices.
			for (int index = 0; index < indices.Length; index++)
			{
				int charIndex = indices[index];
				int n = 0;
				int offset = 0;

				// Select the proper range for the charIndex and subtract lower ranges from it.
				// This will translate the overall index to the range-specific index.
				while (n < ranges.Count)
					if (charIndex > ranges[n++])
						charIndex -= (int)ranges[n - 1];
					else
						break;
				// Find the offset for the digit.  (E.G. index points to 3 in 1431)
				while (charIndex % n != 0 && offset++ >= 0)
					charIndex++;

				// Term is the isolated number in index points to.  (E.G. The 4 in 14 is the 20th character.
				int term = indices[n - 1] + charIndex / n - 1;
				// Use the offset to find the digit in the number.
				while (offset-- > 0)
					term = int.Parse(term.ToString().Remove(term.ToString().Length - 1));
				product *= int.Parse(term.ToString()[term.ToString().Length - 1].ToString());
			}
			output(product);
		}
		#endregion calculations
	}
}