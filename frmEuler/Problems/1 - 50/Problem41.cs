﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly
once. 

	For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
---------------------------------------------------------------------------------------------------
Answer:			7652413
Execution Time:	41 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem41 : Problem
	{
		#region variables
		// If the sum of all digits in a number is a multiple of 3, it is divisible by 3.
		// Can't have 9 digits, as 9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 = 45.  45 % 3 == 0.
		// Can't have 8 digits, as 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 = 36.  36 % 3 == 0.
		private bool[] primes = EulerMath.getPrimeBooleans(7654321);
		private int index = 0;
		#endregion variables

		#region calculations
		public Problem41()
		{
			init();
				
			// Start at the highest value in primes.  If index is prime and pandigital, stop.
			for (index = primes.Length - 1; index >= 0; index -= 2)
				if (primes[index] && EulerMath.isPandigital(index.ToString(), excl: "098"))
					break;
			output(index);
		}
		#endregion calculations
	}
}