﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The nth term of the sequence of triangle numbers is given by, t(n) = n(n+1)/2; so the first ten 
triangle numbers are:

	1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and 
adding these values we form a word value. 

	For example, the word value for SKY is 19 + 11 + 25 = 55 = t(10). 

If the word value is a triangle number then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly 
two-thousand common English words, how many are triangle words?
---------------------------------------------------------------------------------------------------
Answer:			162
Execution Time:	5 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem42 : Problem
	{
		#region variables
		private int count = 0;
		private int highestSum = 0;
		private string[] words = EulerString.separate("Problem 42 - words.txt"); 
		private List<int> values = new List<int>();
		#endregion variables

		#region calculations
		public Problem42()
		{
			init();

			// Create a list of character value sums.  Track the highest one for a triangular limit later.
			foreach (string word in words)
			{
				int temp = EulerString.sumCharValues(word);
				values.Add(temp);
				if (highestSum < temp)
					highestSum = temp;
			}
			// Count how many of the words correspond with triangular values.
			for (int index = 0; index < values.Count; index++)
			{
				List<int> triangles = triangulate(highestSum);
				if (triangles.Contains(values[index]))
					count++;
			}
			output(count);
		}
		#endregion calculations

		#region private functions
		/// <summary>Returns a list of trianglar numbers up to a maximum value.</summary>
		private List<int> triangulate(int max)
		{
			List<int> list = new List<int>(){0};
			for (int index = 1; list[list.Count - 1] < max; index++)
				list.Add((int)EulerMath.tri(index));
			return list;
		}
		#endregion private functions
	}
}