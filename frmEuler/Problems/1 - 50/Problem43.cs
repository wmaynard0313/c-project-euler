﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits
0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

	Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

	d2d3d4=406 is divisible by 2
	d3d4d5=063 is divisible by 3
	d4d5d6=635 is divisible by 5
	d5d6d7=357 is divisible by 7
	d6d7d8=572 is divisible by 11
	d7d8d9=728 is divisible by 13
	d8d9d10=289 is divisible by 17

Find the sum of all 0 to 9 pandigital numbers with this property.
---------------------------------------------------------------------------------------------------
Answer:			16695334890
Execution Time:	4032 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem43 : Problem
	{
		#region variables
		private long result = 0;
		private List<int> primes = EulerMath.getPrimes(max: 17);
		private List<string> pans = new List<string>();
		#endregion variables

		#region calculations
		public Problem43()
		{
			init();

			EulerString.getCombos(ref pans, "0987654321");	// Get all pandigital combinations.

			foreach (string s in pans)
			{
				bool broken = false;
				// Test substring divisibility with corresponding prime numbers.
				for (int index = 0; index < s.Length - 3 && !broken; index++)
				{
					int sub = int.Parse(s.Substring(index + 1, 3));
					if (sub % primes[index] != 0)	// If not divisible, stop.
						broken = true;
				}
				if (!broken)						// Number is eligible.  Add it to the sum.
					result += long.Parse(s);
			}
			output(result);
		}
		#endregion calculations
	}
}