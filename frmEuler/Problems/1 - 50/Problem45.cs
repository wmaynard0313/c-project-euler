﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
Triangle, pentagonal, and hexagonal numbers are generated by the following formulae:

	Triangle	 	Tn=n(n+1)/2	 	1, 3, 6, 10, 15, ...
	Pentagonal	 	Pn=n(3n-1)/2	1, 5, 12, 22, 35, ...
	Hexagonal	 	Hn=n(2n-1)	 	1, 6, 15, 28, 45, ...
	
	It can be verified that T(285) = P(165) = H(143) = 40755.

Find the next triangle number that is also pentagonal and hexagonal.
---------------------------------------------------------------------------------------------------
Answer:			1533776805
Execution Time:	6 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem45 : Problem
	{
		#region variables
		private long result = 0;
		private long t = 285;
		#endregion variables

		#region calculations
		public Problem45()
		{
			init();
			
			// Find the next triangular number that is also pentagonal and hexagonal.
			while (result == 0)
			{
				long temp = EulerMath.tri(++t);
				if (EulerMath.isPent(temp) && EulerMath.isHex(temp))
					result = temp;
			}
			output(result);
		}
		#endregion calculations
	}
}