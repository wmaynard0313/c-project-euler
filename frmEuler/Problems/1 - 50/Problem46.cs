﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of
a prime and twice a square.

	9 = 7 + 2 * 1^2
	15 = 7 + 2 * 2^2
	21 = 3 + 2 * 3^2
	25 = 7 + 2 * 3^2
	27 = 19 + 2 * 2^2
	33 = 31 + 2 * 1^2

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
---------------------------------------------------------------------------------------------------
Answer:			5777
Execution Time:	295 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem46 : Problem
	{
		#region variables
		private int current = 3;
		private List<int> primes = new List<int>() { 2, 3 };
		#endregion variables
		
		#region calculations
		public Problem46()
		{
			init();

			while (true)
				// Keep the highest prime larger than the current number.
				if (primes[primes.Count - 1] <= current)
					EulerMath.addNextPrime(ref primes);
				// Test Goldbach's theorem on the current number.
				else if ((current += 2) != primes[primes.Count - 1])
					if (!goldbach(current, primes))
						break;
			output(current);
		}
		#endregion calculations

		#region private functions
		/// <summary>Test Christian Goldbach's theorem:  Every odd composite number can be written as the sum of a prime and twice a square.</summary>
		private bool goldbach(int composite, List<int> primes)
		{
			// Try all the primes.
			foreach (int prime in primes)
				// Try all the squares with every prime.  Stop when the calculation exceeds the composite number.
				for (int index = 1, total = 0; total <= composite; index++)
				{
					total = prime + (int)(2 * Math.Pow((double)index, 2));
					if (total == composite)
						return true;
				}
			return false;
		}
		#endregion private functions
	}
}