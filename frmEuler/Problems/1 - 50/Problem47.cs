﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The first two consecutive numbers to have two distinct prime factors are:

	14 = 2 x 7
	15 = 3 x 5

The first three consecutive numbers to have three distinct prime factors are:

	644 = 2 x 7 x 23
	645 = 3 x 5 x 43
	646 = 2 x 17 x 19

Find the first four consecutive integers to have four distinct primes factors. What is the first of
these numbers?
---------------------------------------------------------------------------------------------------
Answer:			134043
Execution Time:	3181 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem47 : Problem
	{
		#region variables
		private int index = 3;
		#endregion variables

		#region calculations
		public Problem47()
		{
			init();

			// Find four consecutive numbers with 4 distinct prime factors.
			while (true)
				if (EulerMath.primeFactor(index++).Count == 4)
					if (EulerMath.primeFactor(index++).Count == 4)
						if (EulerMath.primeFactor(index++).Count == 4)
							if (EulerMath.primeFactor(index++).Count == 4)
								break;
			output(index - 4, int.MaxValue);
		}
		#endregion calculations
	}
}