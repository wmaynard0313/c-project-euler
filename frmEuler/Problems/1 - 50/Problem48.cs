﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
---------------------------------------------------------------------------------------------------
Answer:			9110846700
Execution Time:	51 ms
*/
#endregion problem information

#region references
using System.Numerics;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem48 : Problem
	{
		#region variables
		private BigInteger sum = 0;
		#endregion variables

		#region calculations
		public Problem48()
		{
			init();

			// Sum every number below 1,000 to the power of itself.
			for (int index = 1; index <= 1000; index++)
				sum += BigInteger.Pow(index, index);
			// Output the last 10 digits of the result.
			output(sum.ToString().Substring(sum.ToString().Length - 10));
		}
		#endregion calculations
	}
}