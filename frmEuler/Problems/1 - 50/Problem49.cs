﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual
in two ways: 
	(i) each of the three terms are prime
	(ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this 
property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
---------------------------------------------------------------------------------------------------
Answer:			296962999629
Execution Time:	679 ms
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem49 : Problem
	{
		#region variables
		private int index = 0;
		private string result = "";
		private List<int> primes = EulerMath.getPrimes(max: 9999);	// List of primes below 10,000.
		private List<List<int>> primeList = new List<List<int>>();	// List of list of primes with similar digits.
		#endregion variables

		#region calculations
		public Problem49()
		{
			init();

			while (primes[++index] < 1488) ;	// We are given that the first prime is > 1487.

			// Move up through the list of primes.
			for (int x = index; x < primes.Count; x++)
			{
				primeList.Add(new List<int>(){primes[x]});	// Create a list for each new prime.

				// Move up through each individual primes' list.
				for (int y = 0; y < primeList.Count; y++)
					// If the current prime has the same digits as the first prime in the list, add it to the list.
					if (EulerMath.hasSameDigits(primeList[y][0], primes[x]) && !primeList[y].Contains(primes[x]))
						primeList[y].Add(primes[x]);
			}
			
			// Move up through the list of lists.
			for (int x = 0; x < primeList.Count; x++)
				// Find the primes in the list of lists which are in a linear sequence.
				for (int y = 1; primeList[x].Count > 1 && y < primeList[x].Count; y++)
					// If the list contains the first prime, second prime, and first + difference, it's the result.
					if (primeList[x].Contains(primeList[x][y] + (primeList[x][y] - primeList[x][0])))
						result = 
							primeList[x][0].ToString() + 
							primeList[x][y].ToString() + 
							(primeList[x][y] + (primeList[x][y] - primeList[x][0])).ToString();
			output(result);
		}
		#endregion calculations
	}
}