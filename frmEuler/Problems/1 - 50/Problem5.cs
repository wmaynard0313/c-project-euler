﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any
remainder.  

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
---------------------------------------------------------------------------------------------------
Answer:			232792560
Execution Time:	90 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem5 : Problem
	{
		#region variables
		private bool done = false;
		private int result = 0;
		#endregion variables

		#region calculations
		public Problem5()
		{
			init();

			// Since the number must be divisible by 20, move up in increments of 20.
			while(!done)
			{
				result += 20;
				done = true;
				// Mod numbers higher than 10 - numbers below ten are all a factor of a number between 11-19.
				for (int index = 19; done && index >= 11; index--)
					if (result % index != 0)
					{
						done = false;
						break;
					}
			}
			output(result);
		}
		#endregion calculations
	}
}