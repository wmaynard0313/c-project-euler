﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The prime 41, can be written as the sum of six consecutive primes:

	41 = 2 + 3 + 5 + 7 + 11 + 13

This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms,
and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
---------------------------------------------------------------------------------------------------
Answer:			997651
Execution Time:	7489 ms
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem50 : Problem
	{
		#region variables
		private int best = 0;
		private int bestPrime = 0;
		private List<int> primes = EulerMath.getPrimes(max: 1000000);
		private Queue<int> q = new Queue<int>();
		#endregion variables

		#region calculations
		public Problem50()
		{
			init();

			// Start from the largest prime number below one million.
			for (int index = primes.Count - 1; primes[index] > 990000; index--, q.Clear())
				// Move up the prime numbers consecutively.
				for (int x = 0; x < Math.Sqrt(primes[index]); x++)
				{
					if (EulerMath.sum(q) < primes[index])	// Add the next prime number if value not exceeded.
						q.Enqueue(primes[x]);
					if (primes[index] == EulerMath.sum(q))	// Sum of the queue is equal to the prime.
					{
						if (best < q.Count)				// Record it if it's the best result.
						{
							best = q.Count;
							bestPrime = primes[index];
						}
					}
					while (EulerMath.sum(q) > primes[index])	// Queue sum has exceeded the prime.  Remove values.
						q.Dequeue();
				}
			output(bestPrime);
		}
		#endregion calculations
	}
}