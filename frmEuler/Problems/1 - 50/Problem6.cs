﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
The sum of the squares of the first ten natural numbers is:
	1^2 + 2^2 + ... + 10^2 = 385.

The square of the sum of the first ten natural numbers is: 
	(1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square
of the sum is 3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the
square of the sum.
---------------------------------------------------------------------------------------------------
Answer:			25164150
Execution Time:	0 ms
*/
#endregion problem information

#region references
using System;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem6 : Problem
	{
		#region variables
		private int sum = 0;
		private int sumOfSquares = 0;
		#endregion variables

		#region calculations
		public Problem6()
		{
			init();

			for (int index = 1; index <= 100; sum += index++)
				sumOfSquares += (int)Math.Pow(index, 2);
			output((sum * sum) - sumOfSquares);
		}
		#endregion calculations
	}
}