﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
By listing the first six prime numbers: 

	2, 3, 5, 7, 11, and 13, 
 
we can see that the 6th prime is 13.  

What is the 10001st prime number?
---------------------------------------------------------------------------------------------------
Answer:			104743
Execution Time:	424 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem7 : Problem
	{
		#region variables
		#endregion variables

		#region calculations
		public Problem7()
		{
			init();

			output(EulerMath.getPrimes(count: 10001)[10000]);
		}
		#endregion calculations
	}
}