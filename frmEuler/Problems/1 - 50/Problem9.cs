﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
A Pythagorean triplet is a set of three natural numbers, a  b  c, for which a^2 + b^2 = c^2.
	For example: 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.  Find the product abc.
---------------------------------------------------------------------------------------------------
Answer:			31875000
Execution Time:	1 ms
*/
#endregion problem information

#region references
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem9 : Problem
	{
		#region variables
		private int a = 1, b = 1, c = 1;
		private bool done = false;
		#endregion variables

		#region calculations
		public Problem9()
		{
			init();

			// Initialize c after decrementing b.  If c^2 = a^2 + b^2, we're done.
			// Otherwise, if b has fallen below a, increment a and reinitialize b.
			while (!done)
				if ((c = 1000 - (a + --b)) * c == a * a + b * b)
					done = true;
				else if (b <= a)
					b = 1000 - ++a;
			output(a * b * c);
		}
		#endregion calculations
	}
}