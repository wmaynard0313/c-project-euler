﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
By replacing the 1st digit of *3, it turns out that six of the nine possible values: 

	13, 23, 43, 53, 73, and 83,

are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first
example having seven primes among the ten generated numbers, yielding the family:

	56003, 56113, 56333, 56443, 56663, 56773, and 56993. 

Consequently 56003, being the first member of this family, is the smallest prime with this
property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits)
with the same digit, is part of an eight prime value family.
---------------------------------------------------------------------------------------------------
Answer:			121313
Execution Time:	413 ms
Start Date:		8 Jan 2013, 10:36
Finish Date:	8 Jan 2013, 12:34
*/
#endregion problem information

#region references
using System.Collections.Generic;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem51 : Problem
	{
		#region variables
		private bool done = false;
		private bool[] prime = EulerMath.getPrimeBooleans(1000000);
		private bool[][] fives = new bool[][]{
			new bool[]{false, true, true, true},
			new bool[]{true, false, true, true},
			new bool[]{true, true, false, true},
			new bool[]{true, true, true, false}};
		private bool[][] sixes = new bool[][]{
			new bool[]{false, false, true, true, true},
			new bool[]{false, true, false, true, true},
			new bool[]{false, true, true, false, true},
			new bool[]{false, true, true, true, false},
			new bool[]{true, false, false, true, true},
			new bool[]{true, false, true, false, true},
			new bool[]{true, false, true, true, false},
			new bool[]{true, true, false, false, true},
			new bool[]{true, true, false, true, false},
			new bool[]{true, true, true, false, false}};
		private int p = 10001;
		private List<int> family = new List<int>();
		#endregion variables

		#region calculations
		public Problem51()
		{
			init();
			
			while (!done)
				if (prime[p += 2])
				{
					int swapMax = p < 100000 ? fives.Length : sixes.Length;
					// Move through each staggered array of booleans.
					for (int swapIndex = 0; !done && swapIndex < swapMax; swapIndex++)
					{
						// Move through each digit replacement.
						for (int s = 0, temp = 0; s < 10; s++)
						{
							if (p < 100000)
								temp = substitute(p, s, fives[swapIndex]);
							else
								temp = substitute(p, s, sixes[swapIndex]);
							if (prime[temp] && temp.ToString().Length == p.ToString().Length)
								family.Add(temp);
						}
						if (family.Count == 8)
							done = true;
						else
							family.Clear();
					}
				}
			output(family[0]);
		}
		#endregion calculations

		#region private functions
		/// <summary>Takes a boolean array.  For each true value, it substitutes a character for the corresponding digit.  Returns the new integer.</summary>
		private int substitute(int num, int i, bool[] toSwap)
		{
			string output = "";
			string s = num.ToString();
			char c = i.ToString()[0];
			for (int index = 0; index < toSwap.Length; index++)
				if (toSwap[index])
					output += c;
				else
					output += s[index];
			output += s[s.Length - 1];

			return int.Parse(output);
		}
		#endregion private functions
	}
}