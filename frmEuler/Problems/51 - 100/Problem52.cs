﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits,
but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
---------------------------------------------------------------------------------------------------
Answer:			142857
Execution Time:	186 ms
Start Date:		8 Jan 2013, 14:42
Finish Date:	8 Jan 2013, 15:48
*/
#endregion problem information

#region references
using System;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem52 : Problem
	{
		#region variables
		private bool broken = true;
		private int result = 10;
		#endregion variables

		#region calculations
		public Problem52()
		{
			init();

			 // The first two digits can never exceed the value of 16.
			 // 17 * 6 = 102, which means that it will have more digits than the original number.
			while (broken)
			{
				broken = false;
				// Skip ahead to the next set of valid characters.
				if (int.Parse((++result).ToString().Substring(0, 2)) > 16)
					result = (int)Math.Pow(10, result.ToString().Length);
				// Test for same digits.
				for (int index = 2; index <= 6 && !broken; index++)
					if (!EulerMath.hasSameDigits(result, result * index))
						broken = true;
			}
			output(result);
		}
		#endregion calculations
	}
}