﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
There are exactly ten ways of selecting three from five, 12345:

	123, 124, 125, 134, 135, 145, 234, 235, 245, and 345

In combinatorics, we use the notation, 5C(3) = 10.

In general,

	nC(r) =	n! / (r!(n - r)!), where r <= n, n! = n x (n - 1) x ... x 3 x 2 x 1, and 0! = 1.

It is not until n = 23, that a value exceeds one-million: 23C(10) = 1144066.

How many, not necessarily distinct, values of  nC(r), for 1 <= n <= 100, are greater than
one-million?
---------------------------------------------------------------------------------------------------
Answer:			4075
Execution Time:	609 ms
Start Date:		8 Jan 2013, 16:00
Finish Date:	8 Jan 2013, 16:19
*/
#endregion problem information

#region references
using Euler.Library;
using System.Numerics;
#endregion references

namespace Euler.Problems
{
	class Problem53 : Problem
	{
		#region variables
		private int count = 0;
		#endregion variables

		#region calculations
		public Problem53()
		{
			init();

			for (int n = 1; n <= 100; n++)
				for (int r = 1; r <= n; r++)
					if (EulerBigInt.toBigInt(EulerBigInt.ncr(n, r)) > 1000000)
						count++;
			output(count);
		}
		#endregion calculations
	}
}