﻿#region problem information
/*
Problem Text:
---------------------------------------------------------------------------------------------------
In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in
the following way:

High Card: Highest value card.
One Pair: Two cards of the same value.
Two Pairs: Two different pairs.
Three of a Kind: Three cards of the same value.
Straight: All cards are consecutive values.
Flush: All cards of the same suit.
Full House: Three of a kind and a pair.
Four of a Kind: Four cards of the same value.
Straight Flush: All cards are consecutive values of same suit.
Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.

If two players have the same ranked hands then the rank made up of the highest value wins. 
For example, a pair of eights beats a pair of fives (see example 1 below).
But if two ranks tie, for example, both players have a pair of queens, then highest cards in each
hand are compared (see example 4 below); if the highest cards tie then the next highest cards are
compared, and so on.

Consider the following five hands dealt to two players:

Hand	Player 1	 		Player 2	 		Winner
1	 	5H 5C 6S 7S KD		2C 3S 8S 8D TD		Player 2
		Pair of Fives		Pair of Eights

2	 	5D 8C 9S JS AC		2C 5C 7D 8S QH		Player 1
		Ace-High			Queen-High	

3	 	2D 9C AS AH AC		3D 6D 7D TD QD		Player 2
		Three Aces			Flush with Diamonds

4	 	4D 6S 9H QH QC		3D 6D 7H QD QS		Player 1
		Pair of Queens		Pair of Queens
		Nine-High			Seven-High

5	 	2H 2D 4C 4D 4S		3C 3D 3S 9S 9D		Player 1
		Full House			Full House
		With Three Fours	with Three Threes

The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file
contains ten cards (separated by a single space): the first five are Player 1's cards and the last
five are Player 2's cards. You can assume that all hands are valid (no invalid characters or
repeated cards), each player's hand is in no specific order, and in each hand there is a clear
winner.

How many hands does Player 1 win?
---------------------------------------------------------------------------------------------------
Answer:			
Execution Time:	
Start Date:		8 Jan 2013, 16:38
Finish Date:	
*/
#endregion problem information

#region references
using System;
using System.Collections.Generic;
using System.IO;
using Euler.Library;
#endregion references

namespace Euler.Problems
{
	class Problem54 : Problem
	{
		#region variables
		#endregion variables

		#region calculations

/*	
Hand Title			Best Possible Hand	|	Bonus			|	Max Points

High Card			14, 13, 12, 11,  9	|					|	59
One Pair			14, 14, 13, 12, 11	|	59				|	123
Two Pairs			14, 14, 13, 13, 12	|	59 + 59			|	184			// Two pair bonuses.
Three of a Kind		14, 14, 14, 13, 12	|	184				|	251
Straight			14, 13, 12, 11, 10	|	251				|	311
Flush				14, 13, 12, 11,  9	|	311				|	370			// Flush is the only reason the suit matters.
Full House			14, 14, 14, 13, 13	|	184 + 59 + 370	|	681
Four of a Kind		14, 14, 14, 14, 13	|	681				|	750
Straight Flush		13, 12, 11, 10,  9	|	311 + 251 + 750	|	1367		// Straight, Flush, and Four of a Kind bonuses.
Royal Flush			14, 13, 12, 11, 10	|	311 + 251 + 750 |	1372		// Royal flush is identical to a straight flush, just higher cards.  No need to add another bonus.
*/

		//Probably failing on tie hands, then going to high card.
		public Problem54()
		{
			init();
			string o = "";
			foreach (string s in split("8C TS KC 9H 4S 7D 2S 5D 3S AC", ' '))
				o += s + '\n';

			string[] p1 = new string[] { };
			string[] p2 = new string[] { };
			readHands(ref p1, ref p2);

			
			for (int outdex = 0; outdex < p1.Length; outdex++)
			{
				int[] cardCount = new int[14];
				int[] suitCount = new int[4];

				for (int index = 0; index < p1[outdex].Length; index++)
				{
					cardCount[getCardValue(p1[outdex][index++]) - 1]++;
					suitCount[getSuitValue(p1[outdex][index])]++;
				}
				p1[outdex] = getHandValue(cardCount, suitCount).ToString();
			}
			for (int outdex = 0; outdex < p2.Length; outdex++)
			{
				int[] cardCount = new int[14];
				int[] suitCount = new int[4];

				for (int index = 0; index < p2[outdex].Length; index++)
				{
					cardCount[getCardValue(p2[outdex][index++]) - 1]++;
					suitCount[getSuitValue(p2[outdex][index])]++;
				}
				p2[outdex] = getHandValue(cardCount, suitCount).ToString();
			}
			int count = 0;
			for (int index = 0; index < p1.Length; index++)
				if (int.Parse(p1[index]) > int.Parse(p2[index]))
					count++;

			output(count);
		}
		#endregion calculations

		#region private functions
		private int getHandValue(int[] cards, int[] suits)
		{
			bool trip = false;
			bool dupe = false;
			bool straight = false;
			bool flush = false;

			int total = 0;
			// Add all cards.
			for (int index = 0; index < cards.Length; index++)
				total += cards[index] * (index + 1);
			// Look for X of a kind.
			for (int index = 0; index < cards.Length; index++)
				if (cards[index] % 4 == 0)
					total += 681;
				else if (cards[index] % 3 == 0)
				{
					total += 184;
					trip = true;
				}
				else if (cards[index] % 2 == 0)
				{
					total += 59;
					dupe = true;
				}
			// Full House found.
			if (trip && dupe)
				total += 370;
			//Straight, flush, high
			for (int index = 0; index < cards.Length; index++)
				if (cards[index] == 1)
					try
					{
						if (cards[++index] == 1 && cards[++index] == 1 && cards[++index] == 1 && cards[++index] == 1)
							straight = true;
					}
					catch (Exception) { }
			for (int index = 0; index < suits.Length; index++)
				if (suits[index] == 5)
					flush = true;
			if (straight)
				total += 251;
			if (flush)
				total += 311;
			if (straight && flush)
				total += 750;
			if (!(trip || dupe || straight || flush))
			{
				total = 0;
				for (int index = cards.Length - 1; total == 0; index--)
					if (cards[index] > 0)
						total = index + 1;
			}
			return total;
		}
		private int getSuitValue(char c)
		{
			switch (c)
			{
				case 'H':
					return 0;
				case 'D':
					return 1;
				case 'S':
					return 2;
				case 'C':
					return 3;
				default:
					return 4;
			}
		}
		private int getCardValue(char c)
		{
			switch(c)
			{
				case 'A':
					return 14;
				case 'K':
					return 13;
				case 'Q':
					return 12;
				case 'J':
					return 11;
				case 'T':
					return 10;
				default:
					return EulerMath.charToInt(c);
			}
		}

		/// <summary>Custom file reader as the file is in a unique format.</summary>
		private string[][] readHands(ref string[] p1hands, ref string[] p2hands)
		{
			string[] lines = File.ReadAllLines(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
				@"\Visual Studio 2012\Projects\ProjectEuler\frmEuler\Other\Project Resources\Problem 54 - poker.txt");
			List<string>[] hands = new List<string>[lines.Length];
			p1hands = new string[lines.Length];
			p2hands = new string[lines.Length];

			for (int outdex = 0; outdex < lines.Length; outdex++)
			{
				hands[outdex] = split(lines[outdex], ' ');
				string temp = "";
				for (int index = 0; index < 5; index++)
					p1hands[outdex] += hands[outdex][index];
				temp = "";
				for (int index = 5; index < 10; index++)
					p2hands[outdex] += hands[outdex][index];				
			}
			return null;
		}
		/// <summary>Splits a string by a character, returning an array of strings.  Should not begin or end with the splitting character.</summary>
		public static List<string> split(string input, char c)
		{
			List<string> list = new List<string>();

			string temp = "";
			for (int index = 0; index < input.Length; index++)
				if (input[index] != c)
					temp += input[index];
				else
				{
					list.Add(temp);
					temp = "";
				}
			list.Add(temp);
			return list;
		}
		#endregion private functions
	}
}