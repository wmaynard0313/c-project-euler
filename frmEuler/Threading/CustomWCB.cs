﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Euler.Library
{
	class CustomWCB
	{
		private WaitCallback wcb;
		private object index;

		public CustomWCB(WaitCallback inWCB, object inIndex)
		{
			wcb = inWCB;
			index = inIndex;
		}

		public WaitCallback getWCB() { return wcb; }
		public object getIndex() { return index; }
	}
}