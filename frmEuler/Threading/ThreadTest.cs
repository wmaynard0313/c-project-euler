﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Euler.Library;

namespace Euler.Problems
{
	class ThreadTest
	{
		string rest = "";
		protected ManualResetEvent[] resetEvents;
		protected Stack<CustomWCB> threads;
		protected int threadCount = 0;
		private string res = "";

		public ThreadTest()
		{
			this.threads = new Stack<CustomWCB>();
			this.threadCount = 0;
			this.resetEvents = new ManualResetEvent[10];
			for (int index = 0; index < 10; index++)
				resetEvents[index] = new ManualResetEvent(false);

			queue(fun1, threadCount++);
			queue(fun2, threadCount++);

			process();
		}

		private void fun1(object o)
		{
			for(int i = 0; i < 50000; i++)
				rest += '.';

			resetEvents[(int)o].Set();
		}
		private void fun2(object o)
		{
			for (int j = 0; j < 50000; j++)
				res += '\'';

			resetEvents[(int)o].Set();
		}
		protected void queue(WaitCallback wcb, int index)
		{
			threads.Push(new CustomWCB(wcb, (object)index));
		}
		protected void process()
		{
			int threadCount = threads.Count;
			resetEvents = new ManualResetEvent[threadCount];
			CustomWCB cwcb;

			for (int index = 0; index < threadCount; index++)
			{
				resetEvents[index] = new ManualResetEvent(false);

				cwcb = threads.Pop();
				ThreadPool.QueueUserWorkItem(cwcb.getWCB(), cwcb.getIndex());
			}

			WaitHandle.WaitAll(resetEvents);
		}

		protected void finished(object index)
		{
			resetEvents[(int)index].Set();
		}
	}
}

